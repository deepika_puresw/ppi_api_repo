package com.ppi.spouse;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="ppi_claim_spouse") 
public class Spouse {

	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "comman_sequence")
	@SequenceGenerator(name="comman_sequence", sequenceName="comman_sequence",allocationSize=1)
	private int id; 

	@Column(name="claim_id")
	private int claimId;
	
	@Column(name="name")
	private String name;
	
	@Column(name="family_card_number")
	private long familyCardNumber;
	
	@Column(name="updated_by")
	private int updatedBy;

	@Column(name="updated_date")
	private Date updatedDate;

	@Column(name="ktp_number")
	private String ktpNumber;
	
	
	@Column(name="gender")
	private String gender;


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getClaimId() {
		return claimId;
	}


	public void setClaimId(int claimId) {
		this.claimId = claimId;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public long getFamilyCardNumber() {
		return familyCardNumber;
	}


	public void setFamilyCardNumber(long familyCardNumber) {
		this.familyCardNumber = familyCardNumber;
	}


	public int getUpdatedBy() {
		return updatedBy;
	}


	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}


	public Date getUpdatedDate() {
		return updatedDate;
	}


	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}


	public String getKtpNumber() {
		return ktpNumber;
	}


	public void setKtpNumber(String ktpNumber) {
		this.ktpNumber = ktpNumber;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	@Override
	public String toString() {
		return "Spouse [id=" + id + ", claimId=" + claimId + ", name=" + name + ", familyCardNumber=" + familyCardNumber
				+ ", updatedBy=" + updatedBy + ", updatedDate=" + updatedDate + ", ktpNumber=" + ktpNumber + ", gender="
				+ gender + "]";
	}
	
	
}
