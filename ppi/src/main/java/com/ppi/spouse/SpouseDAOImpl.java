package com.ppi.spouse;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;


@Repository
public class SpouseDAOImpl implements SpouseDAO{

	
	@PersistenceContext	
	private EntityManager entityManager;	
	
	@Override
	public void saveSpouse(Spouse Spouse) {
		
		Session session = entityManager.unwrap(Session.class);
		session.saveOrUpdate(Spouse);
	}
	
	@Override
	public List<Spouse>  getFileDetailsError(int claimId) {
		String hql = "FROM Spouse as fd WHERE fd.claimId = :claimId ";
		 List<Spouse> claims=entityManager.createQuery(hql).setParameter("claimId", claimId).getResultList();
		 entityManager.clear();
		return claims;	
	}

	@Override
	public void updateSpouse(Spouse spouse, int claimId) {
		String hql = "Update Spouse as fd WHERE fd.claimId = :claimId ";
		entityManager.createQuery(hql).setParameter("claimId", claimId).getResultList();
	}
}
