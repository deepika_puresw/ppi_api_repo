package com.ppi.spouse;

import java.util.List;

public interface SpouseDAO {

	public void saveSpouse(Spouse Spouse);
	
	public List<Spouse>  getFileDetailsError(int claimId);

	public void updateSpouse(Spouse spouse, int claimId);
	
}
