package com.ppi.spouse;

public class SpouseReqRes {

	private String name;
	
	private long familyCardNumber;
	
	private int updatedBy;

	private String ktpNumber;
	
	private String gender;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getFamilyCardNumber() {
		return familyCardNumber;
	}

	public void setFamilyCardNumber(long familyCardNumber) {
		this.familyCardNumber = familyCardNumber;
	}

	public int getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getKtpNumber() {
		return ktpNumber;
	}

	public void setKtpNumber(String ktpNumber) {
		this.ktpNumber = ktpNumber;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}


	
	
}
