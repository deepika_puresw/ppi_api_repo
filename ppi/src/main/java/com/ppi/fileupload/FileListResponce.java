package com.ppi.fileupload;


import com.ppi.core.Utilities;


public class FileListResponce {

	public FileListResponce(File file) {
		this.name=file.getName();
		this.isParsed=file.getIsParsed();
		this.uploadBy="Kushal";
		this.uploadDate=Utilities.dateTimeToString(file.getUploadDate());
		this.exception=file.getException();
		this.id=file.getId();
	}
	
	private int id;
	
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}

	private String name;
	
	private String uploadDate;
	
	
	private String uploadBy;
	
	
	private int isParsed;
	
	
	private String exception;


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getUploadDate() {
		return uploadDate;
	}


	public void setUploadDate(String uploadDate) {
		this.uploadDate = uploadDate;
	}


	public String getUploadBy() {
		return uploadBy;
	}


	public void setUploadBy(String uploadBy) {
		this.uploadBy = uploadBy;
	}


	public int getIsParsed() {
		return isParsed;
	}


	public void setIsParsed(int isParsed) {
		this.isParsed = isParsed;
	}


	public String getException() {
		return exception;
	}


	public void setException(String exception) {
		this.exception = exception;
	}

	
	
	
}
