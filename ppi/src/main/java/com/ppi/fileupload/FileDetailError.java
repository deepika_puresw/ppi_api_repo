package com.ppi.fileupload;

public class FileDetailError {

	private String contractId;
	
	private String status;
	
	private String remark;
	
	public FileDetailError(FileDetails fileDetails) {
		this.contractId=fileDetails.getContractId();
		if(0==fileDetails.getStatus()) {
			this.status="Under Process";
		}else if(1==fileDetails.getStatus()) {
			this.status="Success";
		}else if(2==fileDetails.getStatus()) {
			this.status="Error";
		}
		
		this.remark=fileDetails.getException();
	}


	public String getContractId() {
		return contractId;
	}

	public void setContractId(String contractId) {
		this.contractId = contractId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
	
}
