package com.ppi.fileupload;

import org.springframework.data.jpa.repository.JpaRepository;

public interface FileDetailsDAO extends JpaRepository<FileDetails, Integer>{

}
