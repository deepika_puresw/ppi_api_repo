package com.ppi.fileupload;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

@Repository
public class FileDAOImpl implements FileDAO{

	@PersistenceContext	
	private EntityManager entityManager;	
	
	@Override
	public int saveFile(File file) {
		
		Session session = entityManager.unwrap(Session.class);
		return  (int) session.save(file);
	}
	
	@Override
	public void updateFile(int id,int isParsed) {
		String hql = "UPDATE File as fd SET fd.isParsed=:isParsed  WHERE fd.id = :id ";
		entityManager.createQuery(hql).setParameter("id", id).setParameter("isParsed", isParsed).executeUpdate();
	}
	
	@Override
	public int saveFileDetails(FileDetails fileDetails) {
		
		Session session = entityManager.unwrap(Session.class);
		return  (int) session.save(fileDetails);
	}
	
	@Override
	public void updateFileDetails(FileDetails fileDetails) {
		Session session = entityManager.unwrap(Session.class);
		session.update(fileDetails);
	}
	
	@Override
	public List<File> getAllUploadedDocuments() {
		String hql = "FROM File";
		List<File> file= entityManager.createQuery(hql).getResultList();
		return file;
	}
	
	@Override
	public List<FileDetails>  getFileDetailsError(int fileId) {
		String hql = "FROM FileDetails as fd WHERE fd.fileId = :fileId ";
		 List<FileDetails> claims=entityManager.createQuery(hql).setParameter("fileId", fileId).getResultList();
		return claims;	
	}
}
