package com.ppi.fileupload;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ppi.core.Utilities;


@Entity
@Table(name="ppi_details") 
public class FileDetails {

	 public FileDetails() {
		 
	 }

    public FileDetails(String[] currentRow,int fileId) {
    	this.fileId=fileId;
    	//System.out.println(currentRow.getCell(0).getStringCellValue());
		this.customerName= currentRow[0];//currentRow.getCell(0).getStringCellValue();
		this.maritalStatus=currentRow[1];//currentRow.getCell(1).getStringCellValue();
		this.mobileNo=currentRow[2];//(long) currentRow.getCell(2).getNumericCellValue();
		this.alternatePhoneNo=currentRow[3];
		this.activeDate=currentRow[4];//currentRow.getCell(4.getDateCellValue();
		this.tenure=currentRow[5];//(int) currentRow.getCell(5).getNumericCellValue();
		this.creditAmount= currentRow[6];//currentRow.getCell(6).getNumericCellValue();
		this.dateBirth=currentRow[8];
		this.gender=currentRow[9];//dataFormatter.formatCellValue(currentRow.getCell(9));
		this.email=currentRow[10];//dataFormatter.formatCellValue(currentRow.getCell(10));
		this.ktpId=currentRow[11];//1223;
		//System.out.currentRow[0];//println(dataFormatter.formatCellValue(currentRow.getCell(12)));
		this.contractId=currentRow[12];//Long.parseLong(dataFormatter.formatCellValue(currentRow.getCell(12)));
		this.virtualAcc=currentRow[13];//(long) currentRow.getCell(13).getNumericCellValue();
		this.monthlyInsAmount=currentRow[14];//(long) currentRow.getCell(14).getNumericCellValue();
		this.prodCodeHcid=currentRow[15];//dataFormatter.formatCellValue(currentRow.getCell(15));
		this.firstInsDueDate=currentRow[16];
		this.lastInsDueDate=currentRow[17];
		this.address=currentRow[18];//dataFormatter.formatCellValue(currentRow.getCell(18));
		this.city=currentRow[19];
		this.postalCode=currentRow[20];
		this.cuid=currentRow[21];//(int) currentRow.getCell(14).getNumericCellValue();
		this.regStatus=currentRow[22];//dataFormatter.formatCellValue(currentRow.getCell(20));
		if(null != currentRow[23])
		this.cancellationDate=currentRow[23];
		if(null != currentRow[24])
		this.terminationDate=currentRow[24];
		this.installmentRate=currentRow[25];//currentRow.getCell(23).getNumericCellValue();
		this.salesRoomName=currentRow[26];//dataFormatter.formatCellValue(currentRow.getCell(24));
		this.salesRoomCity=currentRow[27];//dataFormatter.formatCellValue(currentRow.getCell(25));
    }
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "comman_sequence")
    @SequenceGenerator(name="comman_sequence", sequenceName="comman_sequence",allocationSize=1)
	private int id;
	
	@Column(name="file_id")
	private int fileId;
	
	@Column(name="customer_name")
	private String customerName;
	
	@Column(name="marital_status")
	private String maritalStatus;

	@Column(name="mobile_no")
	private String mobileNo;
	
	@Column(name="alternate_phone_no")
	private String  alternatePhoneNo;
	
	@Column(name="active_date")
	private String activeDate;
	
	@Column(name="tenure")
	private String tenure;
	
	@Column(name="credit_amount")
	private String creditAmount;
	
	@Column(name="date_birth")
	private String dateBirth;
	
	@Column(name="gender")
	private  String gender;
	
	@Column(name="email")
	private String email;
	
	@Column(name="ktp_id")
	private String ktpId;
	
	@Column(name="contract_id")
	private String contractId;

	@Column(name="virtual_acc")
	private String virtualAcc;
	
	@Column(name="monthly_ins_amount")
	private String monthlyInsAmount;
	
	@Column(name="prod_code_hcid")
	private String prodCodeHcid;
	
	@Column(name="first_ins_due_date")
	private  String firstInsDueDate;

	@Column(name="last_ins_due_date")
	private String lastInsDueDate;
	
	@Column(name="address")
	private String address;
	
	@Column(name="cuid")
	private String cuid;
	
	@Column(name="reg_status")
	private String regStatus;
	
	@Column(name="cancellation_date")
	private String cancellationDate;
	
	@Column(name="termination_date")
	private String terminationDate;
	
	@Column(name="installment_rate")
	private String installmentRate;
	
	@Column(name="sales_room_name")
	private String salesRoomName;
	
	@Column(name="sales_room_city")
	private String  salesRoomCity;
	
	@Column(name="city")
	private String  city;
	
	@Column(name="postal_code")
	private String  postalCode;
	
	@Column(name="exception")
	private String exception;
	
	private int status;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getFileId() {
		return fileId;
	}

	public void setFileId(int fileId) {
		this.fileId = fileId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getAlternatePhoneNo() {
		return alternatePhoneNo;
	}

	public void setAlternatePhoneNo(String alternatePhoneNo) {
		this.alternatePhoneNo = alternatePhoneNo;
	}

	public String getActiveDate() {
		return activeDate;
	}

	public void setActiveDate(String activeDate) {
		this.activeDate = activeDate;
	}

	public String getTenure() {
		return tenure;
	}

	public void setTenure(String tenure) {
		this.tenure = tenure;
	}

	public String getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(String creditAmount) {
		this.creditAmount = creditAmount;
	}

	public String getDateBirth() {
		return dateBirth;
	}

	public void setDateBirth(String dateBirth) {
		this.dateBirth = dateBirth;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getKtpId() {
		return ktpId;
	}

	public void setKtpId(String ktpId) {
		this.ktpId = ktpId;
	}

	public String getContractId() {
		return contractId;
	}

	public void setContractId(String contractId) {
		this.contractId = contractId;
	}

	public String getVirtualAcc() {
		return virtualAcc;
	}

	public void setVirtualAcc(String virtualAcc) {
		this.virtualAcc = virtualAcc;
	}

	public String getMonthlyInsAmount() {
		return monthlyInsAmount;
	}

	public void setMonthlyInsAmount(String monthlyInsAmount) {
		this.monthlyInsAmount = monthlyInsAmount;
	}

	public String getProdCodeHcid() {
		return prodCodeHcid;
	}

	public void setProdCodeHcid(String prodCodeHcid) {
		this.prodCodeHcid = prodCodeHcid;
	}

	public String getFirstInsDueDate() {
		return firstInsDueDate;
	}

	public void setFirstInsDueDate(String firstInsDueDate) {
		this.firstInsDueDate = firstInsDueDate;
	}

	public String getLastInsDueDate() {
		return lastInsDueDate;
	}

	public void setLastInsDueDate(String lastInsDueDate) {
		this.lastInsDueDate = lastInsDueDate;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCuid() {
		return cuid;
	}

	public void setCuid(String cuid) {
		this.cuid = cuid;
	}

	public String getRegStatus() {
		return regStatus;
	}

	public void setRegStatus(String regStatus) {
		this.regStatus = regStatus;
	}

	public String getCancellationDate() {
		return cancellationDate;
	}

	public void setCancellationDate(String cancellationDate) {
		this.cancellationDate = cancellationDate;
	}

	public String getTerminationDate() {
		return terminationDate;
	}

	public void setTerminationDate(String terminationDate) {
		this.terminationDate = terminationDate;
	}

	public String getInstallmentRate() {
		return installmentRate;
	}

	public void setInstallmentRate(String installmentRate) {
		this.installmentRate = installmentRate;
	}

	public String getSalesRoomName() {
		return salesRoomName;
	}

	public void setSalesRoomName(String salesRoomName) {
		this.salesRoomName = salesRoomName;
	}

	public String getSalesRoomCity() {
		return salesRoomCity;
	}

	public void setSalesRoomCity(String salesRoomCity) {
		this.salesRoomCity = salesRoomCity;
	}

	public String getException() {
		return exception;
	}

	public void setException(String exception) {
		this.exception = exception;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	
	
}
