package com.ppi.fileupload;

import java.util.List;

public interface FileServicec {

	public int saveFile(int userId,String fileName,String userName);
	
	public FileDetails saveFileDetails(FileDetails fileDetails);
	
	public void updateFileDetails(FileDetails fileDetails);
	
	public List<FileListResponce> getAllUploadedDocuments();
	
	public List<FileDetailError> getError(int fileId);
	
	public void updateFile(int id,int isParsed); 
}
