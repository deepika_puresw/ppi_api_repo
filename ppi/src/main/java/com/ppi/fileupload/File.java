package com.ppi.fileupload;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="ppi_file") 
public class File {

	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "comman_sequence")
    @SequenceGenerator(name="comman_sequence", sequenceName="comman_sequence",allocationSize=1)
	@Column(name="id")
	private int id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="uplode_date")
	private Date uploadDate;
	
	
	@Column(name="uplode_by")
	private int uploadBy;
	
	
	@Column(name="is_parsed")
	private int isParsed;
	
	
	@Column(name="exception")
	private String exception;

	@Column(name="user_name")
	private String userName;

	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}




	public Date getUploadDate() {
		return uploadDate;
	}


	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}


	public int getUploadBy() {
		return uploadBy;
	}


	public void setUploadBy(int uploadBy) {
		this.uploadBy = uploadBy;
	}


	public int getIsParsed() {
		return isParsed;
	}


	public void setIsParsed(int isParsed) {
		this.isParsed = isParsed;
	}


	public String getException() {
		return exception;
	}


	public void setException(String exception) {
		this.exception = exception;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	
}
