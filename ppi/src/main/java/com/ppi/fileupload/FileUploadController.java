package com.ppi.fileupload;


import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.ppi.core.Utilities;

@RestController
public class FileUploadController {

	@Autowired
	 ExcelFileReader read;
	
	@Autowired
	FileServicec fs;
	
	@RequestMapping("fileupload")
	 @CrossOrigin("*")
	public Object fileUpload(@RequestPart MultipartFile file,@RequestParam int userId,@RequestParam String userName ) throws IOException, InvalidFormatException {
			file.getName();
			byte[] bytes = file.getBytes();
			Path path = Paths.get("C:/logs/" + file.getOriginalFilename());
            Files.write(path, bytes);
            File fileName=new File("C:/logs/" + file.getOriginalFilename());
            File file2=new File("C:/logs/"+Utilities.uuid()+".CSV");
            fileName.renameTo(file2);
            System.out.println("-------------------------------"+file2.getName());
            read.readFile(file2,userId,userName);
            
		return "Sucess";
	}
	
	@RequestMapping("uploadedFiles")
	 @CrossOrigin("*")
	public Object uploadedFiles(){
		
		//List<com.ppi.fileupload.File> file=fs.getAllUploadedDocuments();
		return fs.getAllUploadedDocuments();
		
	}
	
	@RequestMapping("getFileDetails/{fileId}")
	 @CrossOrigin("*")
	public Object getFileDetails(@PathVariable int fileId ){
		return fs.getError(fileId);
		
	}
}


