package com.ppi.fileupload;


import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.opencsv.CSVReader;
import com.ppi.core.Utilities;
import com.ppi.customer.Customer;
import com.ppi.customer.customerService;
import com.ppi.policy.Policy;
import com.ppi.policy.PolicyService;
import com.ppi.sale.Sale;
import com.ppi.sale.SaleService;

@Component
public class ExcelFileReader {
	
	@Autowired
	FileServicec fileServicec;
	
	@Autowired
	customerService customerSer;
	
	@Autowired
	SaleService saleService;
	
	@Autowired
	PolicyService policyService;
	
	public void readFile(File file,int userId,String userName) throws InvalidFormatException, IOException {
		int saveFile=fileServicec.saveFile(userId, file.getName(),userName);
		CSVReader csvReader = new CSVReader(new FileReader(file), '|');	
		int isparsed=1;
		csvReader.readNext();
		String[] row = null;
         while ((row = csvReader.readNext()) != null) {
             FileDetails fileDetails=fileServicec.saveFileDetails(new FileDetails(row, saveFile));
             String validate=validateFiledetails(row);			
             if(validate.equals("success")) {
             List<Sale> sales= saleService.getsSales(fileDetails.getSalesRoomName(), fileDetails.getSalesRoomCity());
             int saleId;
             Sale sale=new Sale();
             if(sales.isEmpty()) {
            	
            	 sale.setName(fileDetails.getSalesRoomName());
            	 sale.setCity( fileDetails.getSalesRoomCity());
            	 saleId=saleService.saveSales(sale);
              }else {
            	  saleId=sales.get(0).getId();
			}
             
             List<Customer> customers= customerSer.getCustomer(fileDetails.getKtpId());
             Customer c=new Customer();
             int cusId;
             if(!customers.isEmpty()) {
            	c=customers.get(0);
            	customerSer.updateCustomer(this.setCustomerObject(c, fileDetails));
            	cusId=c.getId();
             }else {
            	 cusId=customerSer.saveCustomer(this.setCustomerObject(c, fileDetails));
			}
             
             
            List<Policy> policys=policyService.getPolicyByContractId( fileDetails.getContractId());
            Policy policy=new Policy();
            if(!policys.isEmpty()) {
            	policy=policys.get(0);
            	policy.setId(policys.get(0).getId());
             }
            policyService.savePolicy(this.setPolicyObject(policy, fileDetails, saleId, cusId,userId));
            fileDetails.setStatus(1);
            fileServicec.updateFileDetails(fileDetails);
         }else {
        	 fileDetails.setStatus(2);
        	 fileDetails.setException(validate);
             fileServicec.updateFileDetails(fileDetails);
             isparsed=0;
		}
         }
         fileServicec.updateFile(saveFile, isparsed);
	}

	public Customer setCustomerObject(Customer c,FileDetails fileDetails) {
		c.setAddress(fileDetails.getAddress());
		c.setCuid(fileDetails.getCuid());
		c.setDate_birth(Utilities.sringToDate(fileDetails.getDateBirth()));
		c.setEmail(fileDetails.getEmail());
		c.setGender(fileDetails.getGender());
		c.setMob_no(fileDetails.getMobileNo());
		c.setName(fileDetails.getCustomerName());
		c.setSocial_id(fileDetails.getKtpId());
		c.setVirtual_acc(fileDetails.getVirtualAcc());
		c.setMaritalStatus(fileDetails.getMaritalStatus());
		c.setCity(fileDetails.getCity());
		c.setPostalCode(fileDetails.getPostalCode());
		return c;
	}

	public Policy setPolicyObject(Policy policy,FileDetails fileDetails,int saleId,int cusId,int userId) {
		policy.setActive_date(Utilities.sringToDateTime(fileDetails.getActiveDate()));
        policy.setContract_id(fileDetails.getContractId());
        policy.setCredit_amount(Double.parseDouble(fileDetails.getCreditAmount()));
        policy.setContract_id(fileDetails.getContractId());
        policy.setCustomer_id(cusId);
        policy.setCreated_on(Utilities.getCurrentDateTime());
        policy.setCreated_by(userId);
        policy.setFst_ins_due_date(Utilities.sringToDate(fileDetails.getFirstInsDueDate()));
        //policy.setIddNo();
        policy.setInstallment_rate(Double.parseDouble(fileDetails.getInstallmentRate()));
        policy.setLast_ins_due_date(Utilities.sringToDate(fileDetails.getLastInsDueDate()));
        policy.setMonthly_ins_amount(Double.parseDouble(fileDetails.getMonthlyInsAmount()));
        //policy.setPolicyNo(fileDetails.get);
        policy.setProd_code_hcid(fileDetails.getProdCodeHcid());
        policy.setResgis_status(fileDetails.getRegStatus());
        //policy.setSales(sale);
        policy.setSales_id(saleId);
        policy.setTenure(Integer.parseInt(fileDetails.getTenure()));
        policy.setCancellationDate(Utilities.sringToDate(fileDetails.getCancellationDate()));
        policy.setTerminationDate(Utilities.sringToDate(fileDetails.getTerminationDate()));
		return policy;
	}
	
	public String validateFiledetails(String[] row) {
		if(row.length != 28) {
			return "COLUMNS SHOULD NOT BE 28";
		}
		if(null ==row[0] || row[0].isEmpty() ) {
			return "CUSTOMER NAME SHOULD NOT BE NULL OR EMPTY";
		}
		if(null ==row[1] || row[1].isEmpty() ) {
			return "MARITAL STATUS SHOULD NOT BE NULL";
		}
		if(null ==row[2] || row[2].isEmpty() ) {
			return "MOBILE NO SHOULD NOT BE NULL";
		}
		
		if(null ==row[4] || row[4].isEmpty() ) {
			return "ACTIV DATE SHOULD NOT BE NULL";
		}
		if(null ==row[5] || row[5].isEmpty() ) {
			return "TENOR SHOULD NOT BE NULL";
		}
		if(null ==row[6] || row[6].isEmpty() ) {
			return "CREDIT AMOUNT SHOULD NOT BE NULL";
		}
		if(null ==row[7] || row[7].isEmpty() ) {
			return "PREMIUM AMOUNT SHOULD NOT BE NULL";
		}
		if(null ==row[8] || row[8].isEmpty() ) {
			return "DATE BIRTH SHOULD NOT BE NULL";
		}
		if(null ==row[9] || row[9].isEmpty() ) {
			return "GENDER SHOULD NOT BE NULL";
		}
		if(null ==row[11] || row[11].isEmpty() ) {
			return "KTP ID SHOULD NOT BE NULL";
		}
		if(null ==row[12] || row[12].isEmpty() ) {
			return "CONTRACT ID SHOULD NOT BE NULL";
		}
		if(null ==row[13] || row[13].isEmpty() ) {
			return "VIRTUAL ACC SHOULD NOT BE NULL";
		}
		if(null ==row[14] || row[14].isEmpty() ) {
			return "MONTHLY INS AMOUNT NOT BE NULL";
		}
		if(null ==row[16] || row[16].isEmpty() ) {
			return "FIRST INS DUEDATE NOT BE NULL";
		}
		if(null ==row[17] || row[17].isEmpty() ) {
			return "LAST INS DUEDATE NOT BE NULL";
		}
		if(null ==row[18] || row[18].isEmpty() ) {
			return "ADDRESS NOT BE NULL";
		}
		
		if(null ==row[19] || row[19].isEmpty() ) {
			return "CITY NOT BE NULL";
		}
		
		if(null ==row[20] || row[20].isEmpty() ) {
			return "POSTAL CODE NOT BE NULL";
		}
		if(null ==row[21] || row[21].isEmpty() ) {
			return "CUID  NOT BE NULL";
		}
		if(null ==row[22] || row[22].isEmpty() ) {
			return "REG STATUS NOT BE NULL";
		}
		if(null ==row[25] || row[25].isEmpty() ) {
			return "INSTALLMENT RATE NOT BE NULL";
		}
		if(null ==row[26] || row[26].isEmpty() ) {
			return "SALES ROOM NAME NOT BE NULL";
		}
		
		if(null ==row[27] || row[27].isEmpty() ) {
			return "SALES ROOM CITY NOT BE NULL";
		}
		
		return "success";
	}
	
	
}
