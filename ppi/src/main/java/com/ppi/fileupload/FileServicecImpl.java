package com.ppi.fileupload;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ppi.core.Utilities;

@Service
public class FileServicecImpl implements FileServicec{

	@Autowired
	FileDAO fileDAO;
	
	@Override
	@Transactional
	public int saveFile(int userId,String fileName,String userName) {
		
		File file=new File();
		file.setName(fileName);
		file.setUploadBy(userId);
		file.setUploadDate(Utilities.getCurrentDateTime());
		file.setUserName(userName);
		return fileDAO.saveFile(file);
	}
	
	
	@Override
	@Transactional
	public FileDetails saveFileDetails(FileDetails fileDetails) {
		
		
		int id= fileDAO.saveFileDetails(fileDetails);
		fileDetails.setId(id);
		return fileDetails;
	}
	
	@Override
	@Transactional
	public void updateFileDetails(FileDetails fileDetails) {
		fileDAO.updateFileDetails(fileDetails);
	}
	
	@Override
	@Transactional
	public List<FileListResponce> getAllUploadedDocuments() {
		
		List<File> files=fileDAO.getAllUploadedDocuments();
		List<FileListResponce> responces=new ArrayList<FileListResponce>();
		if(null !=responces)
		for (File file : files) {
			responces.add(new FileListResponce(file));
		}
		
		return responces;
	}
	
	@Override
	@Transactional
	public List<FileDetailError> getError(int fileId) {
		List<FileDetails> fileDetails= fileDAO.getFileDetailsError(fileId);
		List<FileDetailError> detailErrors=new ArrayList<>();
		if(null !=fileDetails) {
			for (FileDetails fileDetail : fileDetails) {
				detailErrors.add(new FileDetailError(fileDetail));
			}
		}
		return detailErrors;
	}


	@Override
	@Transactional
	public void updateFile(int id, int isParsed) {
		fileDAO.updateFile(id, isParsed);
	}
}
