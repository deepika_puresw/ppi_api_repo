package com.ppi.fileupload;

import java.util.List;

public interface FileDAO {

	public int saveFile(File file);
	
	public void updateFile(int id,int isParsed); 
	
	public int saveFileDetails(FileDetails fileDetails);
	
	public void updateFileDetails(FileDetails fileDetails);
	
	public List<File> getAllUploadedDocuments();
	
	public List<FileDetails>  getFileDetailsError(int fileId);
}
