package com.ppi.exception;


import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import com.ppi.core.CommanResponse;


@ControllerAdvice
@RestController
public class ExceptionController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionController.class);

	@ExceptionHandler(Exception.class)
	public Object exceptionHandler(Exception ex) {
		LOGGER.error(ex.toString());
		System.out.println(ex);
		Map<String, Object> map=new HashMap<>();
		CommanResponse response;
		System.out.println("inside exception");
		if(ex instanceof  NullPointerException) {
			response=new CommanResponse(3000,"Error","Object having null value");
		}else if (ex instanceof SQLException) {
			response=new CommanResponse(3000,"Error","SQL exception");
		}else if (ex instanceof NumberFormatException) {
			response=new CommanResponse(3000,"Error","Number formate exception");
		}
		else {
			response=new CommanResponse(3000,"Error","Unknown error");
			return "comman";
		}
		map.put("response", response);
		return map;
	}
	
	
}
