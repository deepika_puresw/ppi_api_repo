package com.ppi.benificiary;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.ppi.spouse.Spouse;

@Repository
public class BenificiaryDAOImpl implements BenificiaryDAO{

	@PersistenceContext	
	private EntityManager entityManager;	
	
	@Override
	public void saveBenificiary(Benificiary benificiary) {
		
		Session session = entityManager.unwrap(Session.class);
		session.saveOrUpdate(benificiary);
	}

	
	@Override
	public List<Benificiary>  getBenificiaryByClaimId(int claimId) {
		String hql = "FROM Benificiary as fd WHERE fd.claimId = :claimId ";
		 List<Benificiary> benificiary=entityManager.createQuery(hql).setParameter("claimId", claimId).getResultList();
		 entityManager.clear();
		return benificiary;	
	}
	
	@Override
	public void updateBenificiary(Benificiary benificiary,int claimId) {
	String hql = "Update Benificiary as fd WHERE fd.claimId = :claimId ";
	entityManager.createQuery(hql).setParameter("claimId", claimId).getResultList();
	}
}
