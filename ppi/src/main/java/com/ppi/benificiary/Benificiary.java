package com.ppi.benificiary;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ppi.core.Utilities;

@Entity
@Table(name="ppi_claim_beneficiary") 
public class Benificiary {

	public  Benificiary() {
	}
	
	public Benificiary(BenificiaryReqRes reqRes,int claimId,int updatedBy) {
	
		this.claimId=claimId;
		this.familyCardNumber=reqRes.getFamilyCardNumber();
		this.ktpNumber=reqRes.getKtpNumber();
		this.bankAccountNumber=reqRes.getBankAccountNumber();
		this.bankName=reqRes.getBankName();
		this.updatedBy=updatedBy;
		this.updatedDate=Utilities.getCurrentDateTime();
		this.name=reqRes.getName();
	}

	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "comman_sequence")
	@SequenceGenerator(name="comman_sequence", sequenceName="comman_sequence",allocationSize=1)
	private int id; 

	@Column(name="claim_id")
	private int claimId;

	@Column(name="family_card_number")
	private int familyCardNumber;


	@Column(name="ktp_number")
	private String ktpNumber;

	@Column(name="bank_account_number")
	private int bankAccountNumber;

	@Column(name="updated_by")
	private int updatedBy;

	@Column(name="updated_date")
	private Date updatedDate;

	@Column(name="name")
	private String name;

	@Column(name="bank_name")
	private String bankName;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getClaimId() {
		return claimId;
	}

	public void setClaimId(int claimId) {
		this.claimId = claimId;
	}

	public int getFamilyCardNumber() {
		return familyCardNumber;
	}

	public void setFamilyCardNumber(int familyCardNumber) {
		this.familyCardNumber = familyCardNumber;
	}

	public String getKtpNumber() {
		return ktpNumber;
	}

	public void setKtpNumber(String ktpNumber) {
		this.ktpNumber = ktpNumber;
	}

	public int getBankAccountNumber() {
		return bankAccountNumber;
	}

	public void setBankAccountNumber(int bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	public int getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	@Override
	public String toString() {
		return "Benificiary [id=" + id + ", claimId=" + claimId + ", familyCardNumber=" + familyCardNumber
				+ ", ktpNumber=" + ktpNumber + ", bankAccountNumber=" + bankAccountNumber + ", updatedBy=" + updatedBy
				+ ", updatedDate=" + updatedDate + ", name=" + name + ", bankName=" + bankName + "]";
	}



}
