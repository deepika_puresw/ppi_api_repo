package com.ppi.benificiary;

import java.util.Date;

public class BenificiaryReqRes {

	private int familyCardNumber;
	
	private String ktpNumber;

	private int bankAccountNumber;

	private int updatedBy;

	private Date updatedDate;

	private String name;

	private String bankName;

	public int getFamilyCardNumber() {
		return familyCardNumber;
	}

	public void setFamilyCardNumber(int familyCardNumber) {
		this.familyCardNumber = familyCardNumber;
	}

	public String getKtpNumber() {
		return ktpNumber;
	}

	public void setKtpNumber(String ktpNumber) {
		this.ktpNumber = ktpNumber;
	}

	public int getBankAccountNumber() {
		return bankAccountNumber;
	}

	public void setBankAccountNumber(int bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	public int getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	
	
}
