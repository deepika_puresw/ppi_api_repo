package com.ppi.benificiary;

import java.util.List;

public interface BenificiaryDAO {
	public void saveBenificiary(Benificiary benificiary);
	
	public List<Benificiary>  getBenificiaryByClaimId(int claimId);
	public void updateBenificiary(Benificiary benificiary,int claimId) ;
	
}
