package com.ppi.core;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public final class Utilities {

	static DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	static DateFormat dft = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	
	public static String dateToString(Date date) {		
		return df.format(date);
	}
	
	public static Date sringToDate(String date) {		
		try {
			return df.parse(date);
		} catch (ParseException e) {
			return null;
		}
	}

	public static Date getCurrentDate() {	
		Date date = Calendar.getInstance().getTime();
		String today = df.format(date);
		try {
			return  df.parse(today);
		} catch (ParseException e) {
			return null;
		} 
	}

	public static String dateTimeToString(Date date) {		
		return dft.format(date);
	}
	
	public static Date sringToDateTime(String date) {		
		try {
			return dft.parse(date);
		} catch (ParseException e) {
			return null;
		}
	}

	public static Date getCurrentDateTime() {	
		Date date = Calendar.getInstance().getTime();
		String today = dft.format(date);
		try {
			return  dft.parse(today);
		} catch (ParseException e) {
			return null;
		} 
	}

	public static String uuid() {
	    final String uuid = UUID.randomUUID().toString();
	    return uuid;
	}
	
}
