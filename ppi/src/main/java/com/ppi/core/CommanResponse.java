package com.ppi.core;

public class CommanResponse {

	private int code;
	
	private String message;
	
	private String type;
	
	public CommanResponse() {
	}

	public CommanResponse(int code, String type,String message) {
		this.code=code;
		this.message=message;
		this.type=type;
	}

	
	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
	
}
