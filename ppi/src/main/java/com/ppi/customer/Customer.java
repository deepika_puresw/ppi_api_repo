package com.ppi.customer;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.ppi.policy.Policy;

@Entity
@Table(name="ppi_customer")  
public class Customer { 
	
	 
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "comman_sequence")
    @SequenceGenerator(name="comman_sequence", sequenceName="comman_sequence",allocationSize=1)
	@Column(name="id")
	private int id; 


	@Column(name="name")
	private String name; 

	@Column(name="mob_no")
	private String mob_no;
	
	@Column(name="date_birth")
	
	private Date date_birth;

	
	@Column(name="gender")
	private String gender;

	@Column(name="email")
	private String email;
	
	@Column(name="social_id")
	private String social_id;
	
	@Column(name="cuid")
	private String cuid;
	
	@Column(name="virtual_acc")
	private String virtual_acc;
	
	
	@Column(name="address")
	private String address;
	
	@Column(name="alternate_phone_no")
	private String alternatePhoneNo;
	
	@Column(name="marital_status")
	private String maritalStatus;
	 //private Policy policy;
	//private String policy_id;
	
	@Column(name="city")
	private String  city;
	
	@Column(name="postal_code")
	private String  postalCode;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	public Date getDate_birth() {
		return date_birth;
	}

	public void setDate_birth(Date date_birth) {
		this.date_birth = date_birth;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSocial_id() {
		return social_id;
	}

	public void setSocial_id(String social_id) {
		this.social_id = social_id;
	}

	public String getCuid() {
		return cuid;
	}

	public void setCuid(String cuid) {
		this.cuid = cuid;
	}

	public String getVirtual_acc() {
		return virtual_acc;
	}

	public void setVirtual_acc(String virtual_acc) {
		this.virtual_acc = virtual_acc;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMob_no() {
		return mob_no;
	}

	public void setMob_no(String mob_no) {
		this.mob_no = mob_no;
	}

	/*public Policy getPolicy() {
		return policy;
	}

	public void setPolicy(Policy policy) {
		this.policy = policy;
	}*/

	

	

	
	@OneToMany(fetch=FetchType.EAGER, mappedBy="customer")
	//@JsonManagedReference
    private List<Policy> policy;
	public String getAlternatePhoneNo() {
		return alternatePhoneNo;
	}

	public void setAlternatePhoneNo(String alternatePhoneNo) {
		this.alternatePhoneNo = alternatePhoneNo;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public List<Policy> getPolicy() {
		
		return policy;
	}

	public void setPolicy(List<Policy> policy) {
		this.policy = policy;
	}
	/*@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id")
    public int getPolicy() {
        return getPolicy();
    }
 
    public void setPolicy(Policy policy) {
        this.policy = policy;
    }*/
}

