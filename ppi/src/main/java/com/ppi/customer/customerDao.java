package com.ppi.customer;

import java.util.List;

import com.ppi.policy.Policy;
import com.ppi.sale.Sale;




public interface customerDao {




	List<Customer> getCustomerDetails(int contract_id);
	List<Policy> polDetails(int contract_id);

	List<Sale> getsalesdetails(int policy_id);




	List<Customer> getCustomersOnpolicySearch(int policyNo, String social_id, String contract_id, String name,String mob_no, String email);
	List<Customer> getCustomerSeachList(String contract_id, String name, String social_id, String mob_no);


	public List<Customer> getCustomer(String socialId);
	public int saveCustomer(Customer customer) ;
	public void updateCustomer(Customer customer);

}
