package com.ppi.customer;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.ppi.policy.Policy;
import com.ppi.sale.Sale;





@Repository
public class customerDaoImpl implements customerDao {

	@PersistenceContext	
	private EntityManager entityManager;	
	
	
	@Override
    public List<Customer> getCustomerDetails(int policy_id) {
		//Session session=entityManager.unwrap(SessionFactory.class).getCurrentSession();  
		
			String query1 = "select c from Customer c "
 					+ "JOIN c.policy p " 
 	             	+ "WHERE p.id = '"+policy_id +"' " ;
			
     		Query query = null;
     		query = entityManager.createQuery(query1);
 	           	    
 		
     		List<Customer> list = query.getResultList();
     		return list;
	}

	@Override
    public List<Policy> polDetails(int policy_id) {
	//	Session session=entityManager.unwrap(SessionFactory.class).getCurrentSession();      
     		Query query = null;
     		query = entityManager.createQuery("select p from Policy p "
 					
 	             	+ "WHERE p.id = '"+policy_id +"' ");
 	           	    
 		
     		List<Policy> list = query.getResultList();
     		return list;
	}
	
	@Override
    public List<Sale> getsalesdetails(int policy_id) {
		//Session session=entityManager.unwrap(SessionFactory.class).getCurrentSession();      
     		Query query = null;
     		
     			query = entityManager.createQuery("select s from Sale s "
     					+ "JOIN s.policy p " 
     	             	+ "WHERE p.id = '"+policy_id +"' ");
     	             	
     			List<Sale> list = query.getResultList();
         		return list;
     		
     		}
	
	
	
	@Override
	public List<Customer> getCustomersOnpolicySearch(int policyNo, String social_id, String contract_id, String name,
			String mob_no, String email) {
		String query1 = null;
		String query2=null;
		String query3=null;
		Query query4=null;
		//Session session= entityManager.unwrap(SessionFactory.class).getCurrentSession(); 
		
		query1 = joinQueryString()+  " p.policyNo = '"+policyNo +"' ";
		query2 = joinQueryString()+  " p.contract_id LIKE '%"+contract_id +"%' ";
		query3 = joinQueryString()+  " p.policyNo LIKE '%"+policyNo +"%' "
             	+  " AND p.contract_id LIKE '%"+contract_id +"%' ";
		if(policyNo!=0&& null!=contract_id&&!contract_id.isEmpty()) {
			if(null != name && !name.isEmpty()) {
				query3 =  query3 + " and c.name LIKE '%"+name +"%' ";
	 		
	 		}
			if(null != email && !email.isEmpty()) {
				query3 = query3 + " and c.email LIKE '%"+email +"%' ";
	 					 		
	 		}
			if(null != mob_no && !mob_no.isEmpty()) {
				query3 = query3 + " and c.mob_no LIKE '%"+mob_no +"%' ";
	 					 		
	 		}
			if(null != social_id && !social_id.isEmpty()) {
				query3 = query3 + " and c.social_id LIKE '%"+social_id +"%' ";
	 					 		
	 		}
			 query4= entityManager.createQuery(query3);
			
		}
		if(policyNo==0&&null!=contract_id&&!contract_id.isEmpty()) {
			if(null != name && !name.isEmpty()) {
				query2 =  query2 + "and c.name LIKE '%"+name +"%' ";
	 		
	 		}
			if(null != email && !email.isEmpty()) {
				query2 = query2 + " and c.email LIKE '%"+email +"%' ";
	 					 		
	 		}
			if(null != mob_no && !mob_no.isEmpty()) {
				query2 = query2 + " and c.mob_no LIKE '%"+mob_no +"%' ";
	 					 		
	 		}
			if(null != social_id && !social_id.isEmpty()) {
				query2 = query2 + " and c.social_id LIKE '%"+social_id +"%' ";
	 					 		
	 		}
			 query4= entityManager.createQuery(query2);
			
		}
		
		
		if((policyNo!=0 &&(contract_id==null ||contract_id.isEmpty() ))) {
			if(null != name && !name.isEmpty()) {
				query1 = query1+" and c.name LIKE '%"+name +"%' ";
	 		
	 		}
			if(null != email && !email.isEmpty()) {
				query1 = query1+" and c.email LIKE '%"+email +"%' ";
	 					 		
	 		}
			if(null != mob_no && !mob_no.isEmpty()) {
				query1 = query1+" and c.mob_no LIKE '%"+mob_no +"%' ";
	 					 		
	 		}
			if(null != social_id && !social_id.isEmpty()) {
				query1 = query1+" and c.social_id LIKE '%"+social_id +"%' ";
	 					 		
	 		}
			query4=entityManager.createQuery(query1);
		}
		if((policyNo==0&&contract_id.isEmpty())||(policyNo==0&&contract_id==null)) {
			String query5=getCustomeDetailsOnpolicyquery(policyNo,social_id,contract_id,name,mob_no,email);
			query4 =entityManager.createQuery(query5);
			
		}
		List<Customer> list = query4.getResultList();
 		return list;
	}
	
	 public String joinQueryString() {
		 String joinString=getCustomeDetailsselectConditions()
					+ "JOIN c.policy p " 
	             	+ "WHERE";
		 return joinString;
	 }
	 
	 
	public String getCustomeDetailsselectConditions() {
		String select = "select c from Customer c  " ;
		return select;
	}

   public String getCustomeDetailsOnpolicyquery(int policyNo, String social_id, String contract_id, String name,
			String mob_no, String email) {
	        String query = getCustomeDetailsselectConditions()+"where";
		int temp=0;
		if(null != name && !name.isEmpty()) {
			temp=1;
 			query =  query + " c.name LIKE '%"+name +"%' ";
 		
 		}
		if(null != email && !email.isEmpty()) {
			if(temp==1) {
				query +=" and ";
			query = query + " c.email LIKE '%"+email +"%' ";
			}
				else 
				
	 			query = query + " c.email LIKE '%"+email +"%' ";
			temp=1;

 					 		
 		}
		if(null != mob_no && !mob_no.isEmpty()) {
			if(temp==1) {
				query +=" and ";
			query = query + " c.mob_no Like '%"+mob_no +"%' ";
		}
				else 
				
	 			query = query + " c.mob_no LIKE '%"+mob_no +"%' ";
			temp=1;

 					 		
 		}
		if(null != social_id && !social_id.isEmpty()) {
			if(temp==1) {
				query +=" and ";
			query = query + " c.social_id LIKE '%"+social_id +"%' ";
		}
				else 
				
	 			query = query + " c.social_id LIKE '%"+social_id +"%' ";
			temp=1;

 					 		
 		}
		return query;
	
}
   
   @Override
   public List<Customer> getCustomerSeachList(String contract_id, String name, String social_id, String mob_no) {
	   
	   String query1 = null;
		Query query4=null;
	//	Session session= entityManager.unwrap(SessionFactory.class).getCurrentSession(); 
		query1 = getCustomeDetailsselectConditions()
					+ "JOIN c.policy p " 
	             	+ "WHERE p.contract_id LIKE '%"+contract_id +"%' ";
		if(null != contract_id && !contract_id.isEmpty()){
			String query2=query1;
			if(null != name && !name.isEmpty()) {
				query2 = query2 + " and c.name LIKE '%"+name +"%' ";
	 		
	 		}
			
			if(null != mob_no && !mob_no.isEmpty()) {
				query2 = query2 + " and c.mob_no LIKE '%"+mob_no +"%' ";
	 					 		
	 		}
			if(null != social_id && !social_id.isEmpty()) {
				query2 = query2 + " and c.social_id LIKE '%"+social_id +"%' ";
	 					 		
	 		}
			 query4=entityManager.createQuery(query2);
			
		}
	   if(contract_id==null ||contract_id.isEmpty()) {
		   String query = getCustomeDetailsselectConditions()+"where";
		   int temp=0;
			//String query1 = null;
			
			if(null != name && !name.isEmpty()) {
				temp=1;
	 			query =  query + " c.name LIKE '%"+name +"%' ";
	 		
	 		}
		
			if(null != mob_no && !mob_no.isEmpty()) {
				if(temp==1) {
					query +=" and ";
				query = query + " c.mob_no LIKE '%"+mob_no +"%' ";
				}
					else 
					
		 			query = query + " c.mob_no LIKE '%"+mob_no +"%' ";
				temp=1;
	 					 		
	 		}
			if(null != social_id && !social_id.isEmpty()) {
				if(temp==1) {
					query +=" and ";
					query = query + " c.social_id LIKE '%"+social_id +"%' ";
				}
					else 
					
		 			query = query + " c.social_id LIKE '%"+social_id +"%' ";
	 					 		
	 		}
			query4= entityManager.createQuery(query);
	   }
		List<Customer> list = query4.getResultList();
 		return list;
}
   
   @Override 
   public List<Customer> getCustomer(String socialId) {
		String hql = "select customer FROM Customer as customer WHERE customer.social_id = :social_id ";
		 List<Customer> claims=entityManager.createQuery(hql,Customer.class).setParameter("social_id", socialId)
        .getResultList();
		return claims;
	}

   
   @Override
	public int saveCustomer(Customer customer) {
		Session session = entityManager.unwrap(Session.class);
		return  (int) session.save(customer);
	}

@Override
public void updateCustomer(Customer customer) {
	Session session = entityManager.unwrap(Session.class);
	session.update(customer);
}
   
  
   
}
	