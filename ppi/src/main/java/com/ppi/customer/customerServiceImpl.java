package com.ppi.customer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ppi.policy.Policy;
import com.ppi.policy.PolicyDetailsResponse;
import com.ppi.sale.Sale;
import com.ppi.sale.SalesDetailsResponse;

@Service
public class customerServiceImpl implements customerService {
	@Autowired
	public customerDaoImpl customerDaoImpl;
	
	
	
	


	
	

	@Override
	@Transactional
	public List<CustomerDetailsResponse> customerDetailsInformation(int policy_id) {
		List<CustomerDetailsResponse> cust = new ArrayList<>();
		
		List<Customer> customerList = customerDaoImpl.getCustomerDetails(policy_id);
		String lastName=null;
		int Id = 0;
		String gender=null;
		String prod_code=null;
		int alternatePhNum=0;
		String bank_name=null;
		int bank_acc_No=0;
		String language=null;
		String eligibilityStaus=null;
		String currency=null;
		String maritalStatus=null;
		String address=null;
		String city=null;
		String province=null;
		int postalCode=0;
		String country=null;
		Date dob=null;
		for (Customer a : customerList) {
        for (Policy policy : a.getPolicy()) {
        	dob=a.getDate_birth();
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		    String dobString = sdf.format(dob);
        	Id = policy.getId();
				
				prod_code=policy.getProd_code_hcid();
				if(policy_id==Id) 
					cust.add(new CustomerDetailsResponse(a.getName(),lastName,dobString,a.getMob_no(),a.getSocial_id(),a.getEmail(),Id,gender,alternatePhNum,bank_name,bank_acc_No,language,eligibilityStaus,currency,maritalStatus,address,city,province,postalCode,country));

             }
        
						
		}
		return cust;
	}

	@Override
	@Transactional
	public List<PolicyDetailsResponse> policyDetailsInformation(int policy_id) {
		List<PolicyDetailsResponse> cust = new ArrayList<>();
		
		List<Policy> customerList = customerDaoImpl.polDetails(policy_id);
		Date policy_start_date=null;
		Date policy_end_date=null;
		int contractId = 0;
		int remaining_tenure=0;
		String prod_code=null;
		Date cancelletion_date=null;
		Date termination_date=null;
		int Id=0;
		for (Policy a : customerList) {
			
			Id=a.getId();
			 if(policy_id==Id) 
			cust.add(new PolicyDetailsResponse(a.getProd_code_hcid(),a.getContract_id(),a.getResgis_status(),policy_start_date,policy_end_date,a.getTenure(),remaining_tenure,cancelletion_date,termination_date));
		
		}
		return cust;
	}

	
	@Override
	@Transactional
	public List<LoanDetailsResponse> loanDetailsInformation(int policy_id) {
		List<LoanDetailsResponse> cust = new ArrayList<>();
		
		List<Policy> customerList = customerDaoImpl.polDetails(policy_id);
		int premium_amount=0;
		int Id=0;
		for (Policy a : customerList) {
			Id = a.getId();
			 if(policy_id==Id) 
			cust.add(new LoanDetailsResponse(a.getCredit_amount(),premium_amount,a.getFst_ins_due_date(),a.getLast_ins_due_date(),a.getMonthly_ins_amount(),a.getInstallment_rate()));
			
			
		}
		return cust;
	}
	
	@Override
	@Transactional
	public List<SalesDetailsResponse> salesDetailsInformation(int policy_id) {
		List<SalesDetailsResponse> cust = new ArrayList<>();
		
		List<Sale> polList = customerDaoImpl.getsalesdetails(policy_id);
		int Id=0;
		
		for (Sale a : polList) {
			cust.add(new SalesDetailsResponse(a.getCity(),a.getName()));
			break;
		}
		return cust;
	}
	
	
	
	@Override
	@Transactional
	public List<CustomerResponse> getPolicySearchDetails(int policyNo,String social_id,String contract_id,String name,String mob_no,String email){
		List<CustomerResponse> cust = new ArrayList<>();
		  List<Customer> customerList = customerDaoImpl.getCustomersOnpolicySearch(policyNo,social_id,contract_id,name,mob_no,email);
		
		  String contractId = null;
			int id=0;
			String prod_code=null;
			String pol_status=null;
			
			int policyNumber=0;
			Date dob=null;
			 Date date = new Date();
			    
			for (Customer a : customerList) {
				
				for (Policy policy : a.getPolicy()) {
					
					contractId = policy.getContract_id();
					id=policy.getId();
					prod_code=policy.getProd_code_hcid();
					
					policyNumber=policy.getPolicyNo();
					
					pol_status=policy.getResgis_status();
					
					if((contract_id==null||contract_id.equals(contractId)) && policyNo==0)
						cust.add(new CustomerResponse(a.getName(),a.getMob_no(),contractId,a.getSocial_id(),a.getEmail(),prod_code,id,a.getGender(),pol_status,policyNumber));
						if((contract_id==null||contract_id.equals(contractId)) && policyNumber==policyNo)
							cust.add(new CustomerResponse(a.getName(),a.getMob_no(),contractId,a.getSocial_id(),a.getEmail(),prod_code,id,a.getGender(),pol_status,policyNumber));
						if((contract_id==null||contract_id.isEmpty()) && policyNo==0)
							cust.add(new CustomerResponse(a.getName(),a.getMob_no(),contractId,a.getSocial_id(),a.getEmail(),prod_code,id,a.getGender(),pol_status,policyNumber));
						if((contract_id==null||contract_id.isEmpty()) && policyNo==policyNumber)
							cust.add(new CustomerResponse(a.getName(),a.getMob_no(),contractId,a.getSocial_id(),a.getEmail(),prod_code,id,a.getGender(),pol_status,policyNumber));
						
				}
				
			}
			
		  return cust;
	}
	
	@Override
	@Transactional
	public List<CustomerResponse> getCustomersSearch(String contract_id, String name, String social_id, String mob_no){
		List<CustomerResponse> cust = new ArrayList<>();
		  List<Customer> customerList = customerDaoImpl.getCustomerSeachList(contract_id,name,social_id,mob_no);
		  String contractId = null;
			int id=0;
			String prod_code=null;
			String pol_status=null;
			int policyNo=0;
			
			for (Customer a : customerList) {
				
				for (Policy policy : a.getPolicy()) {
					
					contractId = policy.getContract_id();
					id=policy.getId();
					prod_code=policy.getProd_code_hcid();
					
					 policyNo=policy.getPolicyNo();
					
					pol_status=policy.getResgis_status();
					if((contract_id==null||contract_id.isEmpty())||contract_id.equals(contractId))
						cust.add(new CustomerResponse(a.getName(),a.getMob_no(),contractId,a.getSocial_id(),a.getEmail(),prod_code,id,a.getGender(),pol_status,policyNo));
					
					}
				
			}
		  
		  return cust;
	}

	
	@Override
	@Transactional
	public List<Customer> getCustomer(String socialId){
		return customerDaoImpl.getCustomer(socialId);
	}

	@Override
	@Transactional
	public int saveCustomer(Customer customer) {
		return customerDaoImpl.saveCustomer(customer);
	}

	@Override
	@Transactional
	public void updateCustomer(Customer customer) {
		customerDaoImpl.updateCustomer(customer);
	}

	
}
