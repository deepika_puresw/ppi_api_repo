package com.ppi.customer;


import java.util.List;

import com.ppi.policy.PolicyDetailsResponse;
import com.ppi.sale.SalesDetailsResponse;

public interface customerService  {
	

	
	List<CustomerDetailsResponse> customerDetailsInformation(int contract_id);
	
	List<PolicyDetailsResponse> policyDetailsInformation(int contract_id);

	List<LoanDetailsResponse> loanDetailsInformation(int contract_id);

	List<SalesDetailsResponse> salesDetailsInformation(int policy_id);
	List<CustomerResponse> getPolicySearchDetails(int policyNo,String social_id,String contract_id,String name,String mob_no,String email);

	List<CustomerResponse> getCustomersSearch(String contract_id, String name, String social_id, String mob_no);
	
	
	public List<Customer> getCustomer(String socialId);
	
	public int saveCustomer(Customer customer) ;

	public void updateCustomer(Customer customer) ;

	
	
}
