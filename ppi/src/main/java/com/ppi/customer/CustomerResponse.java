package com.ppi.customer;

import java.util.Date;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)

public class CustomerResponse {
	
	//@ApiModelProperty(value = "Customer Id", required = true)
	private String name;;
private String prod_code;
	
	private String contract_id;
	private String social_id;
	private String email;
	private int policy_id;
	private String gender;
	private String pol_status;
	
	private int policyNo;
	private int id; 
   
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
	private Date date_birth;
    private String cuid;
	private String virtual_acc;
	private String address;
	private String alternatePhoneNo;
	private String maritalStatus;
	private String  city;
	private String  postalCode;
	

	
private String mob_no;
	public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public Date getDate_birth() {
	return date_birth;
}

public void setDate_birth(Date date_birth) {
	this.date_birth = date_birth;
}

public String getCuid() {
	return cuid;
}

public void setCuid(String cuid) {
	this.cuid = cuid;
}

public String getVirtual_acc() {
	return virtual_acc;
}

public void setVirtual_acc(String virtual_acc) {
	this.virtual_acc = virtual_acc;
}

public String getAddress() {
	return address;
}

public void setAddress(String address) {
	this.address = address;
}

public String getAlternatePhoneNo() {
	return alternatePhoneNo;
}

public void setAlternatePhoneNo(String alternatePhoneNo) {
	this.alternatePhoneNo = alternatePhoneNo;
}

public String getMaritalStatus() {
	return maritalStatus;
}

public void setMaritalStatus(String maritalStatus) {
	this.maritalStatus = maritalStatus;
}

public String getCity() {
	return city;
}

public void setCity(String city) {
	this.city = city;
}

public String getPostalCode() {
	return postalCode;
}

public void setPostalCode(String postalCode) {
	this.postalCode = postalCode;
}

public void setPolicy_id(int policy_id) {
	this.policy_id = policy_id;
}

	

	public int getPolicy_id() {
		return policy_id;
	}

	/*public void setPolicy_id(int policy_id) {
		this.policy_id = policy_id;
	}*/

	public void setProd_code(String prod_code) {
		this.prod_code = prod_code;
	}

	//@ApiModelProperty(value = "customer mobile number", required = true)
	

	public int getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(int policyNo) {
		this.policyNo = policyNo;
	}

	public String getSocial_id() {
		return social_id;
	}

	public void setSocial_id(String social_id) {
		this.social_id = social_id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContract_id() {
		return contract_id;
	}

	public void setContract_id(String contract_id) {
		this.contract_id = contract_id;
	}

	public String getProd_code() {
		return prod_code;
	}

	public void setPolicy_id(String prod_code) {
		this.prod_code = prod_code;
	}

	public CustomerResponse(String name,String mob_no,String contract_id,String social_id,String email,String prod_code,int policy_id,String gender,String pol_status,int policyNo){
		this.name=name;
		this.prod_code=prod_code;
		this.mob_no=mob_no;
		this.contract_id=contract_id;
		this.social_id=social_id;
		this.email=email;
		this.policy_id=policy_id;
		this.gender=gender;
		this.pol_status=pol_status;
		
		this.policyNo=policyNo;
	}
	
	public CustomerResponse(String name,String social_id,String email,String gender,int id,Date date_birth,String cuid,String virtual_acc,String address,String alternatePhoneNo,String maritalStatus,String city,String postalCode,String mob_no)
	{
		this.name=name;
		this.social_id=social_id;
		this.email=email;
		this.gender=gender;
		this.id=id;
		this.date_birth=date_birth;
		this.cuid=cuid;
		this.virtual_acc=virtual_acc;
		this.address=address;
		this.alternatePhoneNo=alternatePhoneNo;
		this.maritalStatus=maritalStatus;
		this.city=city;
		this.postalCode=postalCode;
		this.mob_no=mob_no;
	}
	
	
	public String getPol_status() {
		return pol_status;
	}

	public void setPol_status(String pol_status) {
		this.pol_status = pol_status;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public CustomerResponse() {
		// TODO Auto-generated constructor stub
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	


	public String getMob_no() {
		return mob_no;
	}

	public void setMob_no(String mob_no) {
		this.mob_no = mob_no;
	}

	
}
