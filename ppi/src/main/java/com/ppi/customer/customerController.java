package com.ppi.customer;

import java.util.HashMap;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class customerController {
	
	 //private final Logger logger = Logger.getLogger(customerController.class);

	@Autowired
    customerService customerService;
	
	@CrossOrigin("*")
  @RequestMapping(value = "/getPolicySearch", method = RequestMethod.POST)
	
	public @ResponseBody Map<String, Object> postController(@RequestBody CustomerResponse cr) {
	    HashMap<String,Object> map=new HashMap<String, Object>();
	   int customer_id=0;
	   // map.put("message", new msgRes(200,"successful","ok"));
	 
	    map.put("customer",customerService.getPolicySearchDetails(cr.getPolicyNo(),cr.getSocial_id(),cr.getContract_id(),cr.getName(),cr.getMob_no(),cr.getEmail()));
	   
	    
		    
    	return map;
    	
	}

  @RequestMapping("/viewPolicyDetails/{id}")
  @CrossOrigin("*")
	public @ResponseBody Map<String, Object> policyDetails(@PathVariable("id") int policy_id) {
	    HashMap<String,Object> map=new HashMap<String, Object>();
	   int customer_id=0;
	   // map.put("message", new msgRes(200,"successful","ok"));
	    int sales_id=0;
	    map.put("customerInformation",customerService.customerDetailsInformation(policy_id));
	    map.put("PolicyInformation",customerService.policyDetailsInformation(policy_id));
	   
	    map.put("LoanInformation",customerService.loanDetailsInformation(policy_id));
	    map.put("SalesInformation",customerService.salesDetailsInformation(policy_id));
	    	
	    
  	return map;
	}

  
  /*@POST
  @Path("searchCustomer")
	 @Produces(MediaType.APPLICATION_JSON)
	 @Consumes(MediaType.APPLICATION_JSON)*/
  @RequestMapping(value = "/searchCustomer", method = RequestMethod.POST)
  @CrossOrigin("*")
	public @ResponseBody Map<String, Object> searchCustomer(@RequestBody CustomerResponse cr) {
	    HashMap<String,Object> map=new HashMap<String, Object>();
	   	    map.put("customer",customerService.getCustomersSearch(cr.getContract_id(),cr.getName(),cr.getSocial_id(),cr.getMob_no()));
		   
		  
  	return map;
	} 
  
  
 /* @GET
  @Path("viewCustomerDetails/{id}")
	 @Produces(MediaType.APPLICATION_JSON)*/
  @RequestMapping("/viewCustomerDetails/{id}")
  @CrossOrigin("*")
	public @ResponseBody Map<String, Object> CustomerInfoDetails(@PathVariable("id") int policy_id) {
	    HashMap<String,Object> map=new HashMap<String, Object>();
	     map.put("customerInformation",customerService.customerDetailsInformation(policy_id));
	    
	    
  	return map;
	}

  
  
  
}