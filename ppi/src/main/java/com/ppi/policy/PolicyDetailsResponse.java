package com.ppi.policy;

import java.util.Date;

public class PolicyDetailsResponse {

	private String prod_code;
	String contract_id;
	String policy_status;
	Date policy_start_date;
	Date policy_end_date;
	int policy_tenure;
	int remaining_tenure;
	Date cancellation_date;
	Date termination_date;
	public PolicyDetailsResponse(String prod_code,String contract_id,String policy_status,Date policy_start_date,Date policy_end_date,int policy_tenure,int remaining_tenure,Date cancelletion_date,Date termination_date ){
		this.prod_code=prod_code;
		this.contract_id=contract_id;
		this.policy_status=policy_status;
		this.policy_start_date=policy_start_date;
		this.policy_end_date=policy_end_date;
		this.policy_tenure=policy_tenure;
		this.remaining_tenure=remaining_tenure;
		this.cancellation_date=cancelletion_date;
		this.termination_date=termination_date;
		
	}
	
	
	public String getContract_id() {
		return contract_id;
	}


	public void setContract_id(String contract_id) {
		this.contract_id = contract_id;
	}


	public String getPolicy_status() {
		return policy_status;
	}


	public void setPolicy_status(String policy_status) {
		this.policy_status = policy_status;
	}


	public Date getPolicy_start_date() {
		return policy_start_date;
	}


	public void setPolicy_start_date(Date policy_start_date) {
		this.policy_start_date = policy_start_date;
	}


	public Date getPolicy_end_date() {
		return policy_end_date;
	}


	public void setPolicy_end_date(Date policy_end_date) {
		this.policy_end_date = policy_end_date;
	}


	public int getPolicy_tenure() {
		return policy_tenure;
	}


	public void setPolicy_tenure(int policy_tenure) {
		this.policy_tenure = policy_tenure;
	}


	public int getRemaining_tenure() {
		return remaining_tenure;
	}


	public void setRemaining_tenure(int remaining_tenure) {
		this.remaining_tenure = remaining_tenure;
	}


	public Date getCancellation_date() {
		return cancellation_date;
	}


	public void setCancellation_date(Date cancellation_date) {
		this.cancellation_date = cancellation_date;
	}


	public Date getTermination_date() {
		return termination_date;
	}


	public void setTermination_date(Date termination_date) {
		this.termination_date = termination_date;
	}


	public String getProd_code() {
		return prod_code;
	}
	public void setProd_code(String prod_code) {
		this.prod_code = prod_code;
	}
	
}
