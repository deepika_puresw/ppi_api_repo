
package com.ppi.policy;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

@Repository
public class PolicyDAOImpl implements PolicyDAO{

	@PersistenceContext	
	private EntityManager entityManager;
	
	@Override
	public List<Policy>  getPolicyByContractId(String ccontractId) {
		String hql = "select policy FROM Policy as policy WHERE policy.contract_id = :ccontractId ";
		 List<Policy> claims=entityManager.createQuery(hql,Policy.class).setParameter("ccontractId", ccontractId).getResultList();
		return claims;	
	}

	@Override
	public void savePolicy(Policy policy) {
		Session session = entityManager.unwrap(Session.class);
		session.saveOrUpdate(policy);
	}

}
