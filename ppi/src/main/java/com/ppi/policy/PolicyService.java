package com.ppi.policy;

import java.util.List;

public interface PolicyService {

		public List<Policy>  getPolicyByContractId(String ccontractId);
		public void savePolicy(Policy policy);
		
}
