package com.ppi.policy;


	

	import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ppi.customer.Customer;
import com.ppi.sale.Sale;  

	@Entity
	@Table(name="ppi_policy")  
	public class Policy { 
		
		@Id
		@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "comman_sequence")
	    @SequenceGenerator(name="comman_sequence", sequenceName="comman_sequence",allocationSize=1)
		@Column(name="id")
		private int id; 

		
		/*@Column(name="claim_id")
		private int claim_id; */

		@Column(name="customer_id")
		private int customer_id;
		
		@Column(name="contract_id")
		private String contract_id;

		
		@Column(name="active_date")
		@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
		private Date active_date;

		@Column(name="tenure")
		private int tenure;
		
		@Column(name="credit_amount")
		private double credit_amount;
		
		@Column(name="monthly_ins_amount")
		private double monthly_ins_amount;
		
		@Column(name="prod_code_hcid")
		private String prod_code_hcid;
		
		
		@Column(name="fst_ins_due_date")
		@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
		private Date fst_ins_due_date;
	
		
		@Column(name="installment_rate")
		private double installment_rate;
		
		@Column(name="last_ins_due_date")
		@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
		private Date last_ins_due_date;
		
		
		@Column(name="resgis_status")
		private String resgis_status;
		
		
		@Column(name="sales_id")
		private int sales_id;
		
		@Column(name="premium_amount")
		private double premiumAmount;
		
		
		@Column(name="cancellation_date")
		@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
		private Date cancellationDate;
		

		@Column(name="termination_date")
		@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
		private Date terminationDate;
		
		
		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		/*public int getClaim_id() {
			return claim_id;
		}

		public void setClaim_id(int claim_id) {
			this.claim_id = claim_id;
		}
*/
		public int getCustomer_id() {
			return customer_id;
		}

		public void setCustomer_id(int customer_id) {
			this.customer_id = customer_id;
		}

		public String getContract_id() {
			return contract_id;
		}

		public void setContract_id(String contract_id) {
			this.contract_id = contract_id;
		}

		public Date getActive_date() {
			return active_date;
		}

		public void setActive_date(Date active_date) {
			this.active_date = active_date;
		}

		public int getTenure() {
			return tenure;
		}

		public void setTenure(int tenure) {
			this.tenure = tenure;
		}

		public double getCredit_amount() {
			return credit_amount;
		}

		public void setCredit_amount(double credit_amount) {
			this.credit_amount = credit_amount;
		}

		public double getMonthly_ins_amount() {
			return monthly_ins_amount;
		}

		public void setMonthly_ins_amount(double monthly_ins_amount) {
			this.monthly_ins_amount = monthly_ins_amount;
		}

		public String getProd_code_hcid() {
			return prod_code_hcid;
		}

		public void setProd_code_hcid(String prod_code_hcid) {
			this.prod_code_hcid = prod_code_hcid;
		}

		/*public Date getFirst_ins_due_date() {
			return first_ins_due_date;
		}

		public void setFirst_ins_due_date(Date first_ins_due_date) {
			this.first_ins_due_date = first_ins_due_date;
		}*/

		public double getInstallment_rate() {
			return installment_rate;
		}

		public Date getFst_ins_due_date() {
			return fst_ins_due_date;
		}

		public void setFst_ins_due_date(Date fst_ins_due_date) {
			this.fst_ins_due_date = fst_ins_due_date;
		}

		public void setInstallment_rate(double installment_rate) {
			this.installment_rate = installment_rate;
		}

		public Date getLast_ins_due_date() {
			return last_ins_due_date;
		}

		public void setLast_ins_due_date(Date last_ins_due_date) {
			this.last_ins_due_date = last_ins_due_date;
		}

		public String getResgis_status() {
			return resgis_status;
		}

		public void setResgis_status(String resgis_status) {
			this.resgis_status = resgis_status;
		}

		public int getSales_id() {
			return sales_id;
		}

		public void setSales_id(int sales_id) {
			this.sales_id = sales_id;
		}

		public int getCreated_by() {
			return created_by;
		}

		public void setCreated_by(int created_by) {
			this.created_by = created_by;
		}

		public Date getCreated_on() {
			return created_on;
		}

		public void setCreated_on(Date created_on) {
			this.created_on = created_on;
		}

		@Column(name="created_usre")
		private int created_by;
		
		@Column(name="created_on")
		private Date created_on;
//		
//		@Column(name="iddNo")
//		private int iddNo;
//		
		
/*
		

		public int getIddNo() {
			return iddNo;
		}

		public void setIddNo(int iddNo) {
			this.iddNo = iddNo;
		}*/

		public int getPolicyNo() {
			return policyNo;
		}

		public void setPolicyNo(int policyNo) {
			this.policyNo = policyNo;
		}

		@Column(name="policyno")
		private int policyNo;
		
		
		//@OneToMany(mappedBy = "ppi_customer", cascade = CascadeType.ALL)
		 
		@ManyToOne(optional=false)
	    @JoinColumn(name="customer_id",referencedColumnName="id", insertable = false, updatable = false)
		@JsonBackReference
	    private Customer customer;

		public Customer getCustomer() {
			return customer;
		}

		public void setCustomer(Customer customer) {
			this.customer = customer;
		}
		
		
		
		 
		@ManyToOne(optional=false)
	    @JoinColumn(name="sales_id",referencedColumnName="id", insertable = false, updatable = false)
		@JsonBackReference
	    private Sale sales;

		public Sale getSales() {
			return sales;
		}

		public void setSales(Sale sales) {
			this.sales = sales;
		}

		public double getPremiumAmount() {
			return premiumAmount;
		}

		public void setPremiumAmount(double premiumAmount) {
			this.premiumAmount = premiumAmount;
		}

		public Date getCancellationDate() {
			return cancellationDate;
		}

		public void setCancellationDate(Date cancellationDate) {
			this.cancellationDate = cancellationDate;
		}

		public Date getTerminationDate() {
			return terminationDate;
		}

		public void setTerminationDate(Date terminationDate) {
			this.terminationDate = terminationDate;
		}

		
}
