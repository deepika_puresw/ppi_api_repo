package com.ppi.policy;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PolicyServiceImpl implements PolicyService{

	@Autowired
	PolicyDAO policyDAO;
	
	@Override
	@Transactional
	public List<Policy> getPolicyByContractId(String contractId) {
		return policyDAO.getPolicyByContractId(contractId);
	}

	@Override
	@Transactional
	public void savePolicy(Policy policy) {
		policyDAO.savePolicy(policy);
	}

}
