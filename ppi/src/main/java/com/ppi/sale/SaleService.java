package com.ppi.sale;

import java.util.List;



public interface SaleService {

	
	public List<Sale> getsSales(String name,String city);
	
	public int saveSales(Sale sale);
}
