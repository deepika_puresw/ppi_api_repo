package com.ppi.sale;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.ppi.policy.Policy;

@Entity
@Table(name="ppi_sales")  
public class Sale { 
		 
		@Id
		@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "comman_sequence")
	    @SequenceGenerator(name="comman_sequence", sequenceName="comman_sequence",allocationSize=1)
		@Column(name="id")
		private int id; 


		@Column(name="name")
		private String name; 

		@Column(name="city")
		private String city;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getCity() {
			return city;
		}

		public void setCity(String city) {
			this.city = city;
		}
		
		@OneToMany(mappedBy="sales",targetEntity=Policy.class, fetch=FetchType.EAGER)
		//@JsonManagedReference
	    private List<Policy> policy;
		public List<Policy> getPolicy() {
			
			return policy;
		}

		public void setPolicy(List<Policy> policy) {
			this.policy = policy;
		}
		
}
