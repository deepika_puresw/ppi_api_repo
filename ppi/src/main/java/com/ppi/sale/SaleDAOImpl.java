package com.ppi.sale;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;


@Repository
public class SaleDAOImpl implements SaleDAO{

	@PersistenceContext	
	private EntityManager entityManager;
	
	 @Override 
	   public List<Sale> getsSales(String name,String city) {
			String hql = "select sale FROM Sale as sale WHERE sale.name = :name and sale.city= :city";
			 List<Sale> claims=entityManager.createQuery(hql,Sale.class).setParameter("name", name).setParameter("city",city)
	        .getResultList();
			return claims;
		}

	@Override
	public int saveSales(Sale sale) {
		Session session = entityManager.unwrap(Session.class);
		return  (int) session.save(sale);
	}
	 
}
