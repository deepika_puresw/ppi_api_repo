package com.ppi.sale;

public class SalesDetailsResponse {
private String name;
private String city;

public SalesDetailsResponse(String city,String name){
	this.name=name;
	this.city=city;

}



public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getCity() {
	return city;
}

public void setCity(String city) {
	this.city = city;
}


}
