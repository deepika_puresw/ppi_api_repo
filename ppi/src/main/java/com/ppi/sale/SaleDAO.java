package com.ppi.sale;

import java.util.List;

public interface SaleDAO {

	public List<Sale> getsSales(String name,String city);
	
	public int saveSales(Sale sale);
}
