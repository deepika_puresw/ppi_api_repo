package com.ppi.sale;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class SaleServiceImpl implements SaleService{
	
	@Autowired
	SaleDAO saleDAO;
	
	@Override
	@Transactional
	public List<Sale> getsSales(String name,String city){
		return saleDAO.getsSales(name, city);
	}

	@Override
	@Transactional
	public int saveSales(Sale sale) {
		// TODO Auto-generated method stub
		return saleDAO.saveSales(sale);
	}
}
