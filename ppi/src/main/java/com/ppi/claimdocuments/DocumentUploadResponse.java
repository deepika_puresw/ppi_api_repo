package com.ppi.claimdocuments;

import com.ppi.core.Utilities;

public class DocumentUploadResponse {
	public DocumentUploadResponse(DocumentUpload documentUpload) {
		
		this.id=documentUpload.getId();
		this.name=documentUpload.getName();
		this.fileType=documentUpload.getFileType();
		this.fileUrl=documentUpload.getFileUrl();
		this.remark=documentUpload.getRemark();
		this.uploadDate=Utilities.dateTimeToString(documentUpload.getUploadDate());
	}

private int id;
	
	private String name;
	
	private String remark;	
	
	private String fileType;
	
	private String uploadDate;
	
	private String fileUrl;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(String uploadDate) {
		this.uploadDate = uploadDate;
	}

	public String getFileUrl() {
		return fileUrl;
	}

	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}
	
	
	
}
