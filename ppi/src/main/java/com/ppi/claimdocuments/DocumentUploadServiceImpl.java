package com.ppi.claimdocuments;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ppi.core.Utilities;

@Service
public class DocumentUploadServiceImpl implements DocumentUploadService{
	
	@Autowired
	DocumentUploadDAO documentUploadDAO;

	@Override
	@Transactional
	public void saveAndUpdate(String fileName,int claimId, String type,  String remark) {

		
		DocumentUpload doc=new DocumentUpload();
		
		doc.setName(fileName);
		doc.setClaimId(claimId);
		doc.setFileType(type);
		doc.setActive(1);
		doc.setRemark(remark);
		doc.setUploadBy(1);
		doc.setUploadDate(Utilities.getCurrentDateTime());
		List<DocumentUpload> documentUploads= documentUploadDAO.checkForClaimIdAndType(claimId,type);
		if(!documentUploads.isEmpty())
		{
			for (DocumentUpload documentUpload : documentUploads) {
				documentUpload.setActive(0);
				documentUploadDAO.saveAndUpdate(documentUpload);
			}
		}
		documentUploadDAO.saveAndUpdate(doc);
}

	@Override
	@Transactional
	public void deleteFile(int id,String type) {
		
		List<DocumentUpload> du=documentUploadDAO.checkForClaimIdAndType(id,type);
		du.get(0).setActive(0);
		documentUploadDAO.saveAndUpdate(du.get(0));
	    
	}
	
	@Override
	@Transactional
	public List<DocumentUploadResponse> listFile(int id) {
		List<DocumentUpload> list=documentUploadDAO.checkForClaimIdAndType(id,null);
		List<DocumentUploadResponse> responses=new ArrayList<DocumentUploadResponse>();
		if(null != list) {
			for (DocumentUpload documentUpload : list) {
				responses.add(new DocumentUploadResponse(documentUpload));
				
			}
		}
		return responses;
		
	}
}
