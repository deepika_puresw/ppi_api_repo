package com.ppi.claimdocuments;

import java.util.List;

public interface DocumentUploadDAO {
	
	public void saveAndUpdate(DocumentUpload doc);
	public List<DocumentUpload> checkForClaimIdAndType(int claimId,String type);
	//public void deleteFile(int claimId,String type);

}