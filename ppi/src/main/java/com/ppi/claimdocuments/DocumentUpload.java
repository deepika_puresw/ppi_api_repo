package com.ppi.claimdocuments;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="ppi_documents") 
public class DocumentUpload {


	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "comman_sequence")
    @SequenceGenerator(name="comman_sequence", sequenceName="comman_sequence",allocationSize=1)
	private int id;
	
	@Column(name="claim_id")
	private Integer claimId;
	
	@Column(name="name")
	private String name;
	
	@Column(name="remark")
	private String remark;	

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name="type")
	private String fileType;
	
	@Column(name="uploded_date")
	private Date uploadDate;
	

	@Column(name="uploded_by")
	private int uploadBy;
	

	@Column(name="is_active")
	private int isActive;
	
	@Column(name="is_archived")
	private int isArchived;
	
	@Column(name="url")
	private String fileUrl;
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getClaimId() {
		return claimId;
	}
	
	public void setClaimId(int claimId) {
		this.claimId = claimId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public Date getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}

	public int getUploadBy() {
		return uploadBy;
	}

	public void setUploadBy(int uploadBy) {
		this.uploadBy = uploadBy;
	}

	public int isActive() {
		return isActive;
	}

	public void setActive(int isActive) {
		this.isActive = isActive;
	}

	public int getIsArchived() {
		return isArchived;
	}

	public void setIsArchived(int isArchived) {
		this.isArchived = isArchived;
	}

	public String getFileUrl() {
		return fileUrl;
	}

	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}

	
}
