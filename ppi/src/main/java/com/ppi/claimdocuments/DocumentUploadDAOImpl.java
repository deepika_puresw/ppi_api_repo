package com.ppi.claimdocuments;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class DocumentUploadDAOImpl implements DocumentUploadDAO{

	@PersistenceContext	
	private EntityManager entityManager;
	
	@Override
	public void saveAndUpdate(DocumentUpload doc) {

		Session session = entityManager.unwrap(Session.class);
		session.saveOrUpdate(doc);
		
	}

	@Override
	public List<DocumentUpload> checkForClaimIdAndType(int claimId,String type) {
		
		//Session session = entityManager.unwrap(Session.class);
		String hql = "FROM DocumentUpload as doc WHERE doc.claimId =:claimId  and doc.isActive=1";
			
		if(null !=type) {
					hql +=" and doc.fileType =:fileType";
				}
		
		Query q =entityManager.createQuery(hql);
		q.setParameter("claimId", claimId);	
		
		if(null !=type) {
			q.setParameter("fileType", type);	
		}

		
		@SuppressWarnings("unchecked")
		List<DocumentUpload> list=q.getResultList();
		return list;
	}

	/*@Override
	public void deleteFile(int claimId, String type) {
		String hql = "delete FROM DocumentUpload as doc WHERE doc.claimId =:claimId and doc.fileType =:type";
		entityManager.createQuery(hql).setParameter("claimId", claimId)
        .setParameter("type", type);
	}*/
	
	

}
