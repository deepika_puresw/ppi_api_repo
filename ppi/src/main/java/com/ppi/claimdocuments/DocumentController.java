package com.ppi.claimdocuments;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.ppi.fileupload.ExcelFileReader;

@RestController
public class DocumentController {
	
	
	 @Autowired
	 ExcelFileReader read;
	 
	 @Autowired
	 DocumentUploadService documentUploadService;
	
	  @RequestMapping("uploadDocument")
	  @CrossOrigin("*")
	  public Object fileUpload(@RequestPart MultipartFile file,@RequestParam int claimId,@RequestParam String fileType,@RequestParam String remark ) throws IOException, InvalidFormatException {
			file.getName();
			byte[] bytes = file.getBytes();
			Path path = Paths.get("C:/logs/Documents/" + file.getOriginalFilename());
           Files.write(path, bytes);
           //File file2=new File("/home/parul/Documents/" +file.getOriginalFilename());
       //  file2.renameTo(new File(pathname)) uuidGenerator 
           
           documentUploadService.saveAndUpdate(file.getName(),claimId,fileType,remark);
           
		return "Success";

}
	  

      @RequestMapping("deleteDocument")
      @CrossOrigin("*")
	  public Object fileDelete(@RequestParam int claimId,@RequestParam String type) throws IOException, InvalidFormatException {
		
		  documentUploadService.deleteFile(claimId,type);
		  return "Success";
    }
	  

	  @RequestMapping("listDocument")
	  @CrossOrigin("*")
	  public List<DocumentUploadResponse> listFile(@RequestParam int claimId) throws IOException, InvalidFormatException {
		
		  List<DocumentUploadResponse> list= documentUploadService.listFile(claimId);
		  return list;
	  
}
}
