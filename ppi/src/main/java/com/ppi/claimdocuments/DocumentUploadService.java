package com.ppi.claimdocuments;

import java.util.List;

public interface DocumentUploadService {

	
	public void saveAndUpdate(String fileName, int id, String type, String remark);
	public void deleteFile(int id,String type);
	public List<DocumentUploadResponse> listFile(int id);

}
