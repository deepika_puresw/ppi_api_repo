package com.ppi.claim;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.ppi.claim.type.Death;
import com.ppi.claim.type.Disability;
import com.ppi.claim.type.Hospitalization;
import com.ppi.claim.type.Illness;

@Repository
public class ClaimDAOImpl implements ClaimDAO{


	@PersistenceContext	
	private EntityManager entityManager;	
	
	@Override
	public int saveClaim(Claim claim) {
		Session session = entityManager.unwrap(Session.class);
		return  (int) session.save(claim);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Claim> getClaimById(int claimId) {
		String hql = "FROM Claim as fd WHERE fd.id = :claimId ";
		 List<Claim> claims=entityManager.createQuery(hql).setParameter("claimId", claimId).getResultList();
		 entityManager.clear();
		return claims ;
	}
	
	

	
	
	@Override
	public void updateClaim(Claim claim,int claimId) {
		Session session = entityManager.unwrap(Session.class);
		session.update(claim);
	}
	
	@Override
	public void saveDeath(Death Death) {
		Session session = entityManager.unwrap(Session.class);
		session.saveOrUpdate(Death);
	}

	@Override
	public void saveDisability(Disability disability) {
		Session session = entityManager.unwrap(Session.class);
		session.saveOrUpdate(disability);
	}
	
	@Override
	public void saveHospitalization(Hospitalization hospitalization) {
		Session session = entityManager.unwrap(Session.class);
		session.saveOrUpdate(hospitalization);
	}
	
	@Override
	public void saveIllness(Illness illness) {
		Session session = entityManager.unwrap(Session.class);
		session.saveOrUpdate(illness);
	}
	
	
	@Override
	public List<Death> getDeath(Death Death,int claimId) {
		
		String hql = "FROM Death as fd WHERE fd.claimId =:claimId ";
		List<Death> deaths= entityManager.createQuery(hql).setParameter("claimId", claimId).getResultList();
		entityManager.clear();
		return deaths;
		}

	@Override
	public List<Disability> getDisability(Disability disability,int claimId) {
		String hql = "FROM Disability as fd WHERE fd.claimId =:claimId ";
		List<Disability> disabilities= entityManager.createQuery(hql).setParameter("claimId", claimId).getResultList();
		entityManager.clear();
		return disabilities;
	}
	
	@Override
	public List<Hospitalization> getHospitalization(Hospitalization hospitalization,int claimId) {
		String hql = "FROM Hospitalization as fd WHERE fd.claimId =:claimId ";
		List<Hospitalization> hospitalizations= entityManager.createQuery(hql).setParameter("claimId", claimId).getResultList();
		entityManager.clear();
		return hospitalizations;
	}
	
	@Override
	public List<Illness> getIllness(Illness illness,int claimId) {
		String hql = "FROM Illness as fd WHERE fd.claimId =:claimId ";
		List<Illness> illnesses= entityManager.createQuery(hql).setParameter("claimId", claimId).getResultList();
		entityManager.clear();
		return illnesses;
	}
	
	@Override

    public List<Claim> getClaimByPolicyId(int policyId) {
                  		Query query = null;

                         query = entityManager.createQuery("select l from Claim l "

                         + "WHERE l.policyId = '"+policyId +"' ");

                         List<Claim> list = query.getResultList();

                         return list;

                  }
	
}
