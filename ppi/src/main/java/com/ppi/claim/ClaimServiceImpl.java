package com.ppi.claim;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ppi.benificiary.Benificiary;
import com.ppi.benificiary.BenificiaryDAO;
import com.ppi.claim.type.Death;
import com.ppi.claim.type.Disability;
import com.ppi.claim.type.Hospitalization;
import com.ppi.claim.type.Illness;
import com.ppi.customer.Customer;
import com.ppi.customer.CustomerResponse;
import com.ppi.customer.customerDao;
import com.ppi.spouse.Spouse;
import com.ppi.spouse.SpouseDAO;

@Service
@Transactional
public class ClaimServiceImpl implements ClaimService{

	@Autowired
	ClaimDAO claimDAO;
	
	@Autowired
	BenificiaryDAO benificiaryDAO;
	
	@Autowired
	SpouseDAO spouseDAO;
	
	
	@Autowired
    customerDao cusDoa;
	
	/*
	@Autowired
	ClaimDocumentsDAO claimDocumentsDAO;
	
	@Override
	@Transactional
	public List<ClaimResponse> getClaimlistByPolicyId(int policyId) {
		List<ClaimResponse>	response=new ArrayList<>();
		List<Claim>  claims=claimDAO.getClaimByPolicyId(policyId);
		if(null !=claims) {
		for (Claim claim : claims) {
			List<ClaimDocuments> claimDocuments=claimDocumentsDAO.getDocumentListByClaimId(claim.getId());
			List<ClaimDocumentResponse> claimDocumentResponses=new ArrayList<>();
			for (ClaimDocuments claimDocument : claimDocuments) {
				claimDocumentResponses.add(new ClaimDocumentResponse(claimDocument));
			}
			response.add(new ClaimResponse(claim,claimDocumentResponses));
		}
		}
		return response;
	}*/

	@Override
	@Transactional
	public int saveClaim(ClaimRequest claimResuest) {
		int claimId=claimResuest.getClaim().getId();
		if(claimId !=0) {
				claimDAO.updateClaim(claimResuest.getClaim(), claimId);
		}else {
			claimId=claimDAO.saveClaim(claimResuest.getClaim());
		}
		claimResuest.getBenificiary().setClaimId(claimId);
		benificiaryDAO.saveBenificiary(claimResuest.getBenificiary());
		claimResuest.getSpouse().setClaimId(claimId);
		spouseDAO.saveSpouse(claimResuest.getSpouse());
		if("Hospitalization".equals(claimResuest.getClaim().getType())) {
			claimResuest.getHospitalization().setClaimId(claimId);
			claimDAO.saveHospitalization(claimResuest.getHospitalization());
		}else if("Critical Illness".equals(claimResuest.getClaim().getType())) {
			claimResuest.getIllness().setClaimId(claimId);
			claimDAO.saveIllness(claimResuest.getIllness());
		}else if("Death".equals(claimResuest.getClaim().getType())) {
			claimResuest.getDeath().setClaimId(claimId);
			claimDAO.saveDeath(claimResuest.getDeath());
		}else if("Total and Permanent Disability".equals(claimResuest.getClaim().getType())) {
			claimResuest.getDisability().setClaimId(claimId);
			claimDAO.saveDisability(claimResuest.getDisability());
		}
		
		return claimId;
	}

	@Override
	@Transactional
	public void updateClaim(Claim claim) {
		claimDAO.updateClaim(claim,1);
	}

	@Override
	@Transactional
	public List<Claim> getClaimById(int claimId) {
		return claimDAO.getClaimById(claimId);
	}

	@Override
	@Transactional
	public List<Hospitalization> getHospitalization(Hospitalization hospitalization, int claimId) {
		return claimDAO.getHospitalization(hospitalization, claimId);
	}

	@Override
	@Transactional
	public List<Illness> getIllness(Illness illness, int claimId) {
		return claimDAO.getIllness(illness, claimId);
	}

	@Override
	@Transactional
	public List<Death> getDeath(Death death, int claimId) {
		return claimDAO.getDeath(death, claimId);
	}

	@Override
	@Transactional
	public List<Disability> getDisability(Disability disability, int claimId) {
		return claimDAO.getDisability(disability, claimId);
	}

	@Override

    @Transactional

    public List<ClaimViewResponse> getClaimlistByPolicyId(int policyId) {
           List<ClaimViewResponse>    response=new ArrayList<>();
           List<Claim>  claims=claimDAO.getClaimByPolicyId(policyId);

           Date reported_date=null;

           int id=0;

           for (Claim a : claims) {

                  id=a.getPolicyId();

                  if(policyId==id)

                         response.add(new ClaimViewResponse(a.getId(),a.getType(),a.getClaimStatus(),a.getDocCompletedBy(),a.getDate()));
           }
           return response;
    }
	
	
	@Override

    @Transactional

   

    public ClaimRequest getClaimDetails(int claimId){

          

           List<Claim> claims= claimDAO.getClaimById(claimId);

           ClaimRequest claimResuest= new ClaimRequest();

           if(null !=claims && !claims.isEmpty()) {

                  Claim claim=claims.get(0);
                  claimResuest.setClaim(claim);
                  List<Benificiary> benificiaries= benificiaryDAO.getBenificiaryByClaimId(claim.getId());

                  if(null !=claims && !claims.isEmpty()) {

                        claimResuest.setBenificiary(benificiaries.get(0));

                  }

                 

                  List<Spouse> spouses=spouseDAO.getFileDetailsError(claim.getId());

                  if(null !=spouses && !spouses.isEmpty()) {

                        claimResuest.setSpouse(spouses.get(0));

                  }

                 

                  if("Hospitalization".equals(claim.getType())) {

                        List<Hospitalization> hospitalizations = claimDAO.getHospitalization(null,claimId);

                        if(null !=hospitalizations && ! hospitalizations.isEmpty()) {

                               claimResuest.setHospitalization(hospitalizations.get(0));

                        }

                  }else if("Critical Illness".equals(claim.getType())) {

                        List<Illness> illnesses= claimDAO.getIllness(null,claimId);

                        if(null !=illnesses && ! illnesses.isEmpty()) {

                               claimResuest.setIllness(illnesses.get(0));

                        }

                  }else if("Death".equals(claim.getType())) {

                        List<Death> deaths= claimDAO.getDeath(null,claimId);

                        if(null !=deaths && ! deaths.isEmpty()) {

                               claimResuest.setDeath(deaths.get(0));

                        }

                  }else if("Total and Permanent Disability".equals(claim.getType())) {

                        List<Disability> disabilities= claimDAO.getDisability(null,claimId);

                        if(null !=disabilities && ! disabilities.isEmpty()) {

                               claimResuest.setDisability(disabilities.get(0));
                        }

                 }

                  List<Customer> customers =  cusDoa.getCustomerDetails(claim.getPolicyId());
                  CustomerResponse  customerResponse = null;
                 for(Customer c:customers) {
                	 customerResponse = new CustomerResponse(c.getName(),c.getSocial_id(),c.getEmail(),c.getGender(),c.getId(),c.getDate_birth(),c.getCuid(),c.getVirtual_acc(),c.getAddress(),c.getAlternatePhoneNo(),
                			 c.getMaritalStatus(),c.getCity(),c.getPostalCode(),c.getMob_no()) ;
                 }

                  if(null !=customerResponse ) {
                      claimResuest.setCustomer(customerResponse);
                  }
    }
           return claimResuest;

    }
	
	
}
