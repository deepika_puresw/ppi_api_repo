package com.ppi.claim;

import java.util.List;

import com.ppi.claim.type.Death;
import com.ppi.claim.type.Disability;
import com.ppi.claim.type.Hospitalization;
import com.ppi.claim.type.Illness;

public interface ClaimService {

	//public List<ClaimResponse> getClaimlistByPolicyId(int policyId);
	public int saveClaim(ClaimRequest claim);
	public void updateClaim(Claim claim);
	public List<Claim> getClaimById(int claimId);
	public List<Hospitalization> getHospitalization(Hospitalization hospitalization, int claimId);
	public List<Illness> getIllness(Illness illness, int claimId);
	public List<Death> getDeath(Death death, int claimId);
	public List<Disability> getDisability(Disability disability, int claimId);
	public Object getClaimlistByPolicyId(int policyId);
	ClaimRequest getClaimDetails(int claimId);
	
}
