package com.ppi.claim;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.websocket.server.PathParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ppi.claim.type.Death;
import com.ppi.claim.type.Disability;
import com.ppi.claim.type.Hospitalization;
import com.ppi.claim.type.Illness;
import com.ppi.core.CommanResponse;



@RestController
public class ClaimController {

	@Autowired
	ClaimService claimService;
	
	private static final Logger logger = LoggerFactory.getLogger(ClaimController.class);
	@GetMapping(value="data")
	
	/*	public Object getClaimlistByPolicyId(@PathParam("id") int policyId) {
		Map<Object, Object> map=new HashMap<>();
		logger.info("Inside clam/data id: "+policyId );
		map.put("response", new  CommanResponse(200,"Success","Success"));
		map.put("claims",claimService.getClaimlistByPolicyId(policyId));
		return map;
	}*/

		public Object getClaimDetails(@PathParam("id") int claimId,@PathParam("claimType") String claimType ) {
		Map<Object, Object> map=new HashMap<>();
	/*	logger.info("Inside claim/details/ id: "+claimId+ " claimType: "+claimType);
		map.put("response", new  CommanResponse(200,"Success","Success"));
		map.put("claims",claimService.getClaimlistByPolicyId(claimId));*/
		return map;
	}
	
	
	 @CrossOrigin("*")
	 @RequestMapping(value = "/saveOrUpdateClaim", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
		public Object saveOrUpdateClaim(@RequestBody ClaimRequest claimResuest) {
			Map<Object, Object> map=new HashMap<>();
			int claimId=claimResuest.getClaim().getId();
			if(claimId !=0) {
				List<Claim> claims= claimService.getClaimById(claimId);{
					if(null != claims && !claims.isEmpty()) {
						if("Hospitalization".equals(claimResuest.getClaim().getType())) {
							List<Hospitalization> hospitalizations = claimService.getHospitalization(claimResuest.getHospitalization(),claimId);
							if(null !=hospitalizations && ! hospitalizations.isEmpty()) {
								claimResuest.getHospitalization().setId(hospitalizations.get(0).getId());
							}
						}else if("Critical Illness".equals(claimResuest.getClaim().getType())) {
							List<Illness> illnesses= claimService.getIllness(claimResuest.getIllness(),claimId);
							if(null !=illnesses && ! illnesses.isEmpty()) {
								claimResuest.getIllness().setId(illnesses.get(0).getId());
							}
						}else if("Death".equals(claimResuest.getClaim().getType())) {
							List<Death> deaths= claimService.getDeath(claimResuest.getDeath(),claimId);
							if(null !=deaths && ! deaths.isEmpty()) {
								claimResuest.getDeath().setId(deaths.get(0).getId());
							}
						}else if("Total and Permanent Disability".equals(claimResuest.getClaim().getType())) {
							List<Disability> disabilities= claimService.getDisability(claimResuest.getDisability(),claimId);
							if(null !=disabilities && ! disabilities.isEmpty()) {
								claimResuest.getDisability().setId(disabilities.get(0).getId());
							}
						}
					}
				}
			}
			claimId=claimService.saveClaim(claimResuest);
			
			
			map.put("response", new  CommanResponse(200,"Success","Success"));
			map.put("claimId", claimId);
			return map;
			
		}
		
	     @CrossOrigin("*")

	     @RequestMapping(value = "/getClaimList/{id}", method = RequestMethod.GET)
	          public @ResponseBody Map<String, Object> getClaimlistByPolicyId(@PathVariable("id") int policyId) {

	              HashMap<String,Object> map=new HashMap<String, Object>();
	             // map.put("message", new msgRes(200,"successful","ok"));

	              List<ClaimViewResponse> list123 = (List<ClaimViewResponse>) claimService.getClaimlistByPolicyId(policyId) ;

	              map.put("claimList",claimService.getClaimlistByPolicyId(policyId));

	          return map;
	          }
	     @CrossOrigin("*")

         @RequestMapping(value = "/claims/{id}", method = RequestMethod.GET)

              public @ResponseBody Map<String, Object> policyDetails(@PathVariable("id") int claimId) {

              HashMap<String,Object> map=new HashMap<String, Object>();

              //logger.info("Inside claim/details/ id: "+claimId+ " claimType: "+claimType);

              //map.put("response", new  CommanResponse(200,"Success","Success"));

              map.put("claims",claimService.getClaimDetails(claimId));

              return map;

       }
}
