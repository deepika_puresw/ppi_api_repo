package com.ppi.claim;

import java.util.List;

import com.ppi.claim.type.Death;
import com.ppi.claim.type.Disability;
import com.ppi.claim.type.Hospitalization;
import com.ppi.claim.type.Illness;

public interface ClaimDAO {

	public List<Claim> getClaimById(int id);
	public int saveClaim(Claim claim);
	public void updateClaim(Claim claim,int claimId);
	public void saveDeath(Death Death);
	public void saveDisability(Disability disability);
	public void saveHospitalization(Hospitalization hospitalization);
	public void saveIllness(Illness illness) ;
	public List<Death> getDeath(Death Death,int claimId);
	public List<Disability> getDisability(Disability disability,int claimId) ;
	public List<Hospitalization> getHospitalization(Hospitalization hospitalization,int claimId) ;
	public List<Illness> getIllness(Illness illness,int claimId) ;
	public List<Claim> getClaimByPolicyId(int policyId);
}
