package com.ppi.claim;

import java.util.List;

import com.ppi.core.Utilities;

public class ClaimResponse {

	private int id;
	
	private String reportedDate;
	
	private String claimType;
	
	private String claimStatus;
	
	private String docCompBy;
	
	public ClaimResponse(Claim claim,List<ClaimDocumentResponse> claimDocumentResponses) {
		this.id=claim.getId();
		this.reportedDate=Utilities.dateToString(claim.getDate());
		this.claimType=claim.getType();
		this.claimStatus=claim.getClaimStatus();
		//this.docCompBy=Utilities.dateToString(claim.getDocCompletedBy());
		this.documents=claimDocumentResponses;
	}
	
	List<ClaimDocumentResponse> documents;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getReportedDate() {
		return reportedDate;
	}

	public void setReportedDate(String reportedDate) {
		this.reportedDate = reportedDate;
	}

	public String getClaimType() {
		return claimType;
	}

	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}

	public String getClaimStatus() {
		return claimStatus;
	}

	public void setClaimStatus(String claimStatus) {
		this.claimStatus = claimStatus;
	}

	public String getDocCompBy() {
		return docCompBy;
	}

	public void setDocCompBy(String docCompBy) {
		this.docCompBy = docCompBy;
	}

	public List<ClaimDocumentResponse> getDocuments() {
		return documents;
	}

	public void setDocuments(List<ClaimDocumentResponse> documents) {
		this.documents = documents;
	}
	
	
	
}
