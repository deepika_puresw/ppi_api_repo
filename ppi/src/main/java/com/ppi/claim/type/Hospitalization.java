package com.ppi.claim.type;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="ppi_claim_hospitalization") 
public class Hospitalization {

	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "comman_sequence")
	@SequenceGenerator(name="comman_sequence", sequenceName="comman_sequence",allocationSize=1)
	private int id; 
	
	@Column(name="claim_id")
	private int claimId;
	
	@Column(name="addmission_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy HH:mm:ss")
	private Date addmissionDate;
	
	@Column(name="remarks")
	private String remarks;

	@Column(name="discharge_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy HH:mm:ss")
	private Date dischargeDate;

	@Column(name="no_of_days")
	private int noOfDays;
	
	@Column(name="summary_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy HH:mm:ss")
	private Date summaryDate;
	
	@Column(name="bill_recieved_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy HH:mm:ss")
	private Date billRecievedDate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getClaimId() {
		return claimId;
	}

	public void setClaimId(int claimId) {
		this.claimId = claimId;
	}

	public Date getAddmissionDate() {
		return addmissionDate;
	}

	public void setAddmissionDate(Date addmissionDate) {
		this.addmissionDate = addmissionDate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Date getDischargeDate() {
		return dischargeDate;
	}

	public void setDischargeDate(Date dischargeDate) {
		this.dischargeDate = dischargeDate;
	}

	public int getNoOfDays() {
		return noOfDays;
	}

	public void setNoOfDays(int noOfDays) {
		this.noOfDays = noOfDays;
	}

	public Date getSummaryDate() {
		return summaryDate;
	}

	public void setSummaryDate(Date summaryDate) {
		this.summaryDate = summaryDate;
	}

	public Date getBillRecievedDate() {
		return billRecievedDate;
	}

	public void setBillRecievedDate(Date billRecievedDate) {
		this.billRecievedDate = billRecievedDate;
	}

	@Override
	public String toString() {
		return "Hospitalization [id=" + id + ", claimId=" + claimId + ", addmissionDate=" + addmissionDate
				+ ", remarks=" + remarks + ", dischargeDate=" + dischargeDate + ", noOfDays=" + noOfDays
				+ ", summaryDate=" + summaryDate + ", billRecievedDate=" + billRecievedDate + "]";
	}
	
	
}
