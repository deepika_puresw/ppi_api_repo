package com.ppi.claim.type;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;


@Entity
@Table(name="ppi_claim_death") 
public class Death {

	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "comman_sequence")
	@SequenceGenerator(name="comman_sequence", sequenceName="comman_sequence",allocationSize=1)
	private int id; 
	
	@Column(name="claim_id")
	private int claimId;
	
	@Column(name="certificate_reciept_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy HH:mm:ss")
	private Date certificateRecieptDate;
	
	@Column(name="declaration_recieved_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy HH:mm:ss")
	private Date  declarationRecievedDate;
	
	private String place;
	
	@Column(name="diagnose_place")
	private String diagnosePlace;
	
	private String cause;
	
	@Column(name="due_to_suicide")
	private String dueToSuicide;
	
	@Column(name="beneficiary_ktp_recived_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy HH:mm:ss")
	private Date  beneficiaryKtpRecivedDate;
	
	@Column(name="beneficiary_bank_recieved_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy HH:mm:ss")
	private Date  beneficiaryBankRecievedDate;
	
	@Column(name="nature_of_death")
	private String natureOfDeath;
	
	private String autopsy;
	
	@Column(name="autopsy_detail")
	private String autopsyDetail;
	
	@Column(name="unature_death")
	private String unatureDeath;
	
	@Column(name="police_report")
	private String policeReport;
	
	@Column(name="autopcy_report_recieved_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy HH:mm:ss")
	private Date  autopcyReportRecievedDate;
	
	@Column(name="police_report_recieved_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy HH:mm:ss")
	private Date  policeReportRecievedDate;
	
	@Column(name="burial_cremation")
	private String burialCremation;
	
	@Column(name="burial_cremation_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy HH:mm:ss")
	private Date  burialCremationDate;
	
	@Column(name="death_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy HH:mm:ss")
	private Date  deathDate;
	
	private String location;
	
	private String remarks;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getClaimId() {
		return claimId;
	}

	public void setClaimId(int claimId) {
		this.claimId = claimId;
	}

	public Date getCertificateRecieptDate() {
		return certificateRecieptDate;
	}

	public void setCertificateRecieptDate(Date certificateRecieptDate) {
		this.certificateRecieptDate = certificateRecieptDate;
	}

	public Date getDeclarationRecievedDate() {
		return declarationRecievedDate;
	}

	public void setDeclarationRecievedDate(Date declarationRecievedDate) {
		this.declarationRecievedDate = declarationRecievedDate;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getDiagnosePlace() {
		return diagnosePlace;
	}

	public void setDiagnosePlace(String diagnosePlace) {
		this.diagnosePlace = diagnosePlace;
	}

	public String getCause() {
		return cause;
	}

	public void setCause(String cause) {
		this.cause = cause;
	}

	public String getDueToSuicide() {
		return dueToSuicide;
	}

	public void setDueToSuicide(String dueToSuicide) {
		this.dueToSuicide = dueToSuicide;
	}


	public Date getBeneficiaryKtpRecivedDate() {
		return beneficiaryKtpRecivedDate;
	}

	public void setBeneficiaryKtpRecivedDate(Date beneficiaryKtpRecivedDate) {
		this.beneficiaryKtpRecivedDate = beneficiaryKtpRecivedDate;
	}

	public Date getBeneficiaryBankRecievedDate() {
		return beneficiaryBankRecievedDate;
	}

	public void setBeneficiaryBankRecievedDate(Date beneficiaryBankRecievedDate) {
		this.beneficiaryBankRecievedDate = beneficiaryBankRecievedDate;
	}

	public String getNatureOfDeath() {
		return natureOfDeath;
	}

	public void setNatureOfDeath(String natureOfDeath) {
		this.natureOfDeath = natureOfDeath;
	}

	public String getAutopsy() {
		return autopsy;
	}

	public void setAutopsy(String autopsy) {
		this.autopsy = autopsy;
	}

	public String getAutopsyDetail() {
		return autopsyDetail;
	}

	public void setAutopsyDetail(String autopsyDetail) {
		this.autopsyDetail = autopsyDetail;
	}

	public String getUnatureDeath() {
		return unatureDeath;
	}

	public void setUnatureDeath(String unatureDeath) {
		this.unatureDeath = unatureDeath;
	}

	public String getPoliceReport() {
		return policeReport;
	}

	public void setPoliceReport(String policeReport) {
		this.policeReport = policeReport;
	}

	public Date getAutopcyReportRecievedDate() {
		return autopcyReportRecievedDate;
	}

	public void setAutopcyReportRecievedDate(Date autopcyReportRecievedDate) {
		this.autopcyReportRecievedDate = autopcyReportRecievedDate;
	}

	public Date getPoliceReportRecievedDate() {
		return policeReportRecievedDate;
	}

	public void setPoliceReportRecievedDate(Date policeReportRecievedDate) {
		this.policeReportRecievedDate = policeReportRecievedDate;
	}

	public String getBurialCremation() {
		return burialCremation;
	}

	public void setBurialCremation(String burialCremation) {
		this.burialCremation = burialCremation;
	}

	public Date getBurialCremationDate() {
		return burialCremationDate;
	}

	public void setBurialCremationDate(Date burialCremationDate) {
		this.burialCremationDate = burialCremationDate;
	}

	public Date getDeathDate() {
		return deathDate;
	}

	public void setDeathDate(Date deathDate) {
		this.deathDate = deathDate;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public String toString() {
		return "Death [id=" + id + ", claimId=" + claimId + ", certificateRecieptDate=" + certificateRecieptDate
				+ ", declarationRecievedDate=" + declarationRecievedDate + ", place=" + place + ", diagnosePlace="
				+ diagnosePlace + ", cause=" + cause + ", dueToSuicide=" + dueToSuicide + ", beneficiaryKtpRecivedDate="
				+ beneficiaryKtpRecivedDate + ", beneficiaryBankRecievedDate=" + beneficiaryBankRecievedDate
				+ ", natureOfDeath=" + natureOfDeath + ", autopsy=" + autopsy + ", autopsyDetail=" + autopsyDetail
				+ ", unatureDeath=" + unatureDeath + ", policeReport=" + policeReport + ", autopcyReportRecievedDate="
				+ autopcyReportRecievedDate + ", policeReportRecievedDate=" + policeReportRecievedDate
				+ ", burialCremation=" + burialCremation + ", burialCremationDate=" + burialCremationDate
				+ ", deathDate=" + deathDate + ", location=" + location + ", remarks=" + remarks + "]";
	}
	
	
}
