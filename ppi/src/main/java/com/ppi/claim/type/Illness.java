package com.ppi.claim.type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="ppi_claim_critical_illness") 
public class Illness {

	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "comman_sequence")
	@SequenceGenerator(name="comman_sequence", sequenceName="comman_sequence",allocationSize=1)
	private int id; 
	
	@Column(name="claim_id")
	private int claimId;
	
	@Column(name="extend_of_illness")
	private String extendOfIllness;
	
	@Column(name="biopsy_report")
	private String biopsyReport;
	
	@Column(name="ecg_report")
	private String ecgReport;
	
	@Column(name="mri_report")
	private String mriReport;
	
	@Column(name="leboratory_report")
	private String leboratoryReport;
	
	private String remark;
	
	private String type;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getClaimId() {
		return claimId;
	}

	public void setClaimId(int claimId) {
		this.claimId = claimId;
	}

	public String getExtendOfIllness() {
		return extendOfIllness;
	}

	public void setExtendOfIllness(String extendOfIllness) {
		this.extendOfIllness = extendOfIllness;
	}

	public String getBiopsyReport() {
		return biopsyReport;
	}

	public void setBiopsyReport(String biopsyReport) {
		this.biopsyReport = biopsyReport;
	}

	public String getEcgReport() {
		return ecgReport;
	}

	public void setEcgReport(String ecgReport) {
		this.ecgReport = ecgReport;
	}

	public String getMriReport() {
		return mriReport;
	}

	public void setMriReport(String mriReport) {
		this.mriReport = mriReport;
	}

	public String getLeboratoryReport() {
		return leboratoryReport;
	}

	public void setLeboratoryReport(String leboratoryReport) {
		this.leboratoryReport = leboratoryReport;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Illness [id=" + id + ", claimId=" + claimId + ", extendOfIllness=" + extendOfIllness + ", biopsyReport="
				+ biopsyReport + ", ecgReport=" + ecgReport + ", mriReport=" + mriReport + ", leboratoryReport="
				+ leboratoryReport + ", remark=" + remark + ", type=" + type + "]";
	}
	
	
}
