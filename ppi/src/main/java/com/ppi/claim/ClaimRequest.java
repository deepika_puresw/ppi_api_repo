package com.ppi.claim;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.ppi.benificiary.Benificiary;
import com.ppi.claim.type.Death;
import com.ppi.claim.type.Disability;
import com.ppi.claim.type.Hospitalization;
import com.ppi.claim.type.Illness;
import com.ppi.spouse.Spouse;
import com.ppi.customer.Customer;
import com.ppi.customer.CustomerResponse;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ClaimRequest {

	private Claim claim; 
	
	private Spouse spouse;
	
	private  Benificiary benificiary;
	
	private Death death;
	
	private Disability disability;
	
	private Hospitalization hospitalization;
	
	private Illness illness;
	
	private CustomerResponse customer;

	public CustomerResponse getCustomer() {
		return customer;
	}

	/*public void setCustomer(Customer customer) {
		this.customer = customer;
	}*/

	public Claim getClaim() {
		return claim;
	}

	public void setClaim(Claim claim) {
		this.claim = claim;
	}

	

	public Spouse getSpouse() {
		return spouse;
	}

	public void setSpouse(Spouse spouse) {
		this.spouse = spouse;
	}


	public Benificiary getBenificiary() {
		return benificiary;
	}

	public void setBenificiary(Benificiary benificiary) {
		this.benificiary = benificiary;
	}

	public Death getDeath() {
		return death;
	}

	public void setDeath(Death death) {
		this.death = death;
	}

	public Disability getDisability() {
		return disability;
	}

	public void setDisability(Disability disability) {
		this.disability = disability;
	}

	public Hospitalization getHospitalization() {
		return hospitalization;
	}

	public void setHospitalization(Hospitalization hospitalization) {
		this.hospitalization = hospitalization;
	}

	public Illness getIllness() {
		return illness;
	}

	public void setIllness(Illness illness) {
		this.illness = illness;
	}

	@Override
	public String toString() {
		return "ClaimResuest [claim=" + claim + ", spouse=" + spouse + ", benificiary=" + benificiary + ", death="
				+ death + ", disability=" + disability + ", hospitalization=" + hospitalization + ", illness=" + illness
				+ "]";
	}

	public void setCustomer(CustomerResponse customer) {
		this.customer = customer;
	}

	
	
	
	
	
}
