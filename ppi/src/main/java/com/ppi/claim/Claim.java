package com.ppi.claim;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="ppi_claim") 
public class Claim {

	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "comman_sequence")
    @SequenceGenerator(name="comman_sequence", sequenceName="comman_sequence",allocationSize=1)
	
	private int id; 
	
	@Column(name="policy_id")
	private int policyId;
	
	@Column(name="user_id")
	private int userId;
	
	@Column(name="type")
	private String type;
	
	@Column(name="claim_status")
	private String claimStatus;
	
	@Column(name="date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
	private Date date;
	
	@Column(name="doc_completed_by")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
	private Date docCompletedBy;
	
	/*@Column(name="filed_by")
	private int filedBy;
*/
	
	@Column(name="source")
	private String source;
	
	@Column(name="priority")
	private String priority;
	
	@Column(name="claiment")
	private String claiment;
	
	@Column(name="entitlement")
	private String entitlement;
	
	@Column(name="social_id_received_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
	private Date socialIdReceivedDate;
	
	@Column(name="social_id_verified_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
	private Date socialIdVerifiedDate;
	
	@Column(name="form_received_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
	private Date formReceivedDate;
	
	@Column(name="form_verified_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
	private Date formVerifiedDate;
	
	@Column(name="proposed_by")
	private String proposedBy;
	
	@Column(name="proposed_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
	private Date proposedDate;
	
	@Column(name="claime_approved")
	private String claimApproved;
	
	@Column(name="approved_by")
	private String approvedBy;
	
	
	@Column(name="approval_date_time")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
	private Date approvalDateTime;
	
	@Column(name="status")
	private String status;
	
	@Column(name="claim_amount_paid")
	private double claimAmountPaid;
	
	@Column(name="claim_amount_transfer_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
	private Date claimAmountTransferDate;
	
	@Column(name="primary_reason_delay")
	private String primaryReasonDelay;
	
	@Column(name="cancellation_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
	private Date cancellationDate;
	
	@Column(name="secondary_reason_delay")
	private String secondaryReasonDelay;
	
	@Column(name="cancellation_reason")
	private String cancellationReason;
	
	
	@Column(name="appealed_letter_received_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
	private Date appealedLetterReceivedDate;
	
	@Column(name="claim_closed_date_time")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
	private Date claimClosedDateTime;
	
	@Column(name="appealed")
	private String appealed;
	
	@Column(name="investigation_required")
	private String investigationRequired;
	
	@Column(name="investigation_detail")
	private String investigationDetail;
	
	@Column(name="investigation_completion_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
	private Date investigationCompletionDate;
	
	@Column(name="investigation_outcome_detail")
	private String investigationOutcomeDetails;
	
	@Column(name="medical_report_condition_met")
	private String medicalReportConditionMet;
	
	@Column(name="hospital_name")
	private String hospitalName;
	
	@Column(name="hospital_phone_number")
	private String hospitalPhoneNumber;
	
	@Column(name="hospital_province")
	private String hospitalProvince;
	
	@Column(name="hospital_postal_code")
	private String hospitalPostalCode;
	
	@Column(name="hospital_country")
	private String hospitalCountry;
	
	@Column(name="marriage_certification_received_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
	private Date marriageCertificationReceivedDate;
	
	@Column(name="marriage_certification_verified_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
	private Date marriageCertificationVerifiedDate;
	
	@Column(name="number_of_previous_hospitalisation")
	private int numberOfPreviousHospitalisation;
	
	@Column(name="previous_ci_claim_exist")
	private String previousCiClaimExist;
	
	@Column(name="previous_tpd_claim_exist")
	private String previousTpdClaimExist;
	
	@Column(name="previous_hospitalisation_claim_exist")
	private String previousHospitalisationClaimExist;
	
	@Column(name="benefit_description")
	private String benefitDescription;
	
	@Column(name="benefit_amount")
	private double benefitAmount;
	
	@Column(name="total_payable_benefit")
	private double totalPayableBenefit;
	
	@Column(name="hcid_confirmation_amount")
	private double hcidConfirmationAmount;
	
	@Column(name="hcid_confirmation_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
	private Date hcidConfirmationDate;
	
	@Column(name="hospital_address")
	private String hospitalAddress;
	
	@Column(name="hospital_city")
	private String hospitalCity;
	
	@Column(name="medical_report_detail")
	private String medicalReportDetail;
	
	@Column(name="medical_report_received_date_time")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
	private Date medicalReportReceivedDateTime;
	
	@Column(name="medical_report_verified_date_time")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
	private Date medicalReportVerifiedDateTime;
	
	@Column(name="accident_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
	private Date accidentDate;
	
	@Column(name="accident_location")
	private String accidentLocation;
	
	
	@Column(name="nature_of_injury")
	private String natureOfInjury;
	
	@Column(name="circumstances_of_accident")
	private String circumstancesOfAccident;
	
	@Column(name="doctor_name")
	private String doctorName;
	
	@Column(name="doctor_qualification")
	private String doctorQualification;
	
	@Column(name="doctor_speciality")
	private String doctorSpeciality;
	
	@Column(name="pre_existing_condition_detail")
	private String preExistingConditionDetail;
	
	@Column(name="pre_existing_condition")
	private String preExistingCondition;
	
	@Column(name="diagnostic_detail")
	private String diagnosticDetail;
	
	@Column(name="symptom_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
	private Date symptomDate;
	
	@Column(name="diagnose_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss")
	private Date diagnoseDate;
	
	@Column(name="previous_death_claim_exist")
	private String previousDeathClaimExist;
	
	@Column(name="decline_reason")
	private String declineReason;
	
	@Column(name="doc_received_date")
	private Date docReceivedDate;
	
	@Column(name="decline_date")
	private Date declineDate;
	
	@Column(name="doc_verification_date")
	private Date docVerificationDate;
	
	@Column(name="decision_made_date")
	private Date decisionMadeDate;
	
	@Column(name="claim_transferredto_beneficiary_date")
	private Date claimTransferredtoBeneficiaryDate;
	
	@Column(name="illness_type") 
	private String illnessType;
	
	@Column(name="appealed_date")
	private Date appealedDate;
	
	
	public String getDeclineReason() {
		return declineReason;
	}

	public void setDeclineReason(String declineReason) {
		this.declineReason = declineReason;
	}

	public Date getDocReceivedDate() {
		return docReceivedDate;
	}

	public void setDocReceivedDate(Date docReceivedDate) {
		this.docReceivedDate = docReceivedDate;
	}

	public Date getDeclineDate() {
		return declineDate;
	}

	public void setDeclineDate(Date declineDate) {
		this.declineDate = declineDate;
	}

	public Date getDocVerificationDate() {
		return docVerificationDate;
	}

	public void setDocVerificationDate(Date docVerificationDate) {
		this.docVerificationDate = docVerificationDate;
	}

	public Date getDecisionMadeDate() {
		return decisionMadeDate;
	}

	public void setDecisionMadeDate(Date decisionMadeDate) {
		this.decisionMadeDate = decisionMadeDate;
	}

	public Date getClaimTransferredtoBeneficiaryDate() {
		return claimTransferredtoBeneficiaryDate;
	}

	public void setClaimTransferredtoBeneficiaryDate(Date claimTransferredtoBeneficiaryDate) {
		this.claimTransferredtoBeneficiaryDate = claimTransferredtoBeneficiaryDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPolicyId() {
		return policyId;
	}

	public void setPolicyId(int policyId) {
		this.policyId = policyId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getClaimStatus() {
		return claimStatus;
	}

	public void setClaimStatus(String claimStatus) {
		this.claimStatus = claimStatus;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	

	public Date getDocCompletedBy() {
		return docCompletedBy;
	}

	public void setDocCompletedBy(Date docCompletedBy) {
		this.docCompletedBy = docCompletedBy;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getClaiment() {
		return claiment;
	}

	public void setClaiment(String claiment) {
		this.claiment = claiment;
	}

	public String getEntitlement() {
		return entitlement;
	}

	public void setEntitlement(String entitlement) {
		this.entitlement = entitlement;
	}

	public Date getSocialIdReceivedDate() {
		return socialIdReceivedDate;
	}

	public void setSocialIdReceivedDate(Date socialIdReceivedDate) {
		this.socialIdReceivedDate = socialIdReceivedDate;
	}

	public Date getSocialIdVerifiedDate() {
		return socialIdVerifiedDate;
	}

	public void setSocialIdVerifiedDate(Date socialIdVerifiedDate) {
		this.socialIdVerifiedDate = socialIdVerifiedDate;
	}

	public Date getFormReceivedDate() {
		return formReceivedDate;
	}

	public void setFormReceivedDate(Date formReceivedDate) {
		this.formReceivedDate = formReceivedDate;
	}

	public Date getFormVerifiedDate() {
		return formVerifiedDate;
	}

	public void setFormVerifiedDate(Date formVerifiedDate) {
		this.formVerifiedDate = formVerifiedDate;
	}

	public String getProposedBy() {
		return proposedBy;
	}

	public void setProposedBy(String proposedBy) {
		this.proposedBy = proposedBy;
	}

	public Date getProposedDate() {
		return proposedDate;
	}

	public void setProposedDate(Date proposedDate) {
		this.proposedDate = proposedDate;
	}

	public String getClaimApproved() {
		return claimApproved;
	}

	public void setClaimApproved(String claimApproved) {
		this.claimApproved = claimApproved;
	}

	public String getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}

	public Date getApprovalDateTime() {
		return approvalDateTime;
	}

	public void setApprovalDateTime(Date approvalDateTime) {
		this.approvalDateTime = approvalDateTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public double getClaimAmountPaid() {
		return claimAmountPaid;
	}

	public void setClaimAmountPaid(double claimAmountPaid) {
		this.claimAmountPaid = claimAmountPaid;
	}


	public Date getClaimAmountTransferDate() {
		return claimAmountTransferDate;
	}

	public void setClaimAmountTransferDate(Date claimAmountTransferDate) {
		this.claimAmountTransferDate = claimAmountTransferDate;
	}

	public String getPrimaryReasonDelay() {
		return primaryReasonDelay;
	}

	public void setPrimaryReasonDelay(String primaryReasonDelay) {
		this.primaryReasonDelay = primaryReasonDelay;
	}

	public Date getCancellationDate() {
		return cancellationDate;
	}

	public void setCancellationDate(Date cancellationDate) {
		this.cancellationDate = cancellationDate;
	}

	public String getSecondaryReasonDelay() {
		return secondaryReasonDelay;
	}

	public void setSecondaryReasonDelay(String secondaryReasonDelay) {
		this.secondaryReasonDelay = secondaryReasonDelay;
	}

	public String getCancellationReason() {
		return cancellationReason;
	}

	public void setCancellationReason(String cancellationReason) {
		this.cancellationReason = cancellationReason;
	}

	public Date getAppealedLetterReceivedDate() {
		return appealedLetterReceivedDate;
	}

	public void setAppealedLetterReceivedDate(Date appealedLetterReceivedDate) {
		this.appealedLetterReceivedDate = appealedLetterReceivedDate;
	}

	public Date getClaimClosedDateTime() {
		return claimClosedDateTime;
	}

	public void setClaimClosedDateTime(Date claimClosedDateTime) {
		this.claimClosedDateTime = claimClosedDateTime;
	}

	public String getAppealed() {
		return appealed;
	}

	public void setAppealed(String appealed) {
		this.appealed = appealed;
	}

	public String getInvestigationRequired() {
		return investigationRequired;
	}

	public void setInvestigationRequired(String investigationRequired) {
		this.investigationRequired = investigationRequired;
	}

	public String getInvestigationDetail() {
		return investigationDetail;
	}

	public void setInvestigationDetail(String investigationDetail) {
		this.investigationDetail = investigationDetail;
	}

	public Date getInvestigationCompletionDate() {
		return investigationCompletionDate;
	}

	public void setInvestigationCompletionDate(Date investigationCompletionDate) {
		this.investigationCompletionDate = investigationCompletionDate;
	}

	public String getInvestigationOutcomeDetails() {
		return investigationOutcomeDetails;
	}

	public void setInvestigationOutcomeDetails(String investigationOutcomeDetails) {
		this.investigationOutcomeDetails = investigationOutcomeDetails;
	}

	public String getMedicalReportConditionMet() {
		return medicalReportConditionMet;
	}

	public void setMedicalReportConditionMet(String medicalReportConditionMet) {
		this.medicalReportConditionMet = medicalReportConditionMet;
	}

	public String getHospitalName() {
		return hospitalName;
	}

	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}

	public String getHospitalPhoneNumber() {
		return hospitalPhoneNumber;
	}

	public void setHospitalPhoneNumber(String hospitalPhoneNumber) {
		this.hospitalPhoneNumber = hospitalPhoneNumber;
	}

	public String getHospitalProvince() {
		return hospitalProvince;
	}

	public void setHospitalProvince(String hospitalProvince) {
		this.hospitalProvince = hospitalProvince;
	}

	public String getHospitalPostalCode() {
		return hospitalPostalCode;
	}

	public void setHospitalPostalCode(String hospitalPostalCode) {
		this.hospitalPostalCode = hospitalPostalCode;
	}

	public String getHospitalCountry() {
		return hospitalCountry;
	}

	public void setHospitalCountry(String hospitalCountry) {
		this.hospitalCountry = hospitalCountry;
	}

	public Date getMarriageCertificationReceivedDate() {
		return marriageCertificationReceivedDate;
	}

	public void setMarriageCertificationReceivedDate(Date marriageCertificationReceivedDate) {
		this.marriageCertificationReceivedDate = marriageCertificationReceivedDate;
	}

	public Date getMarriageCertificationVerifiedDate() {
		return marriageCertificationVerifiedDate;
	}

	public void setMarriageCertificationVerifiedDate(Date marriageCertificationVerifiedDate) {
		this.marriageCertificationVerifiedDate = marriageCertificationVerifiedDate;
	}

	public int getNumberOfPreviousHospitalisation() {
		return numberOfPreviousHospitalisation;
	}

	public void setNumberOfPreviousHospitalisation(int numberOfPreviousHospitalisation) {
		this.numberOfPreviousHospitalisation = numberOfPreviousHospitalisation;
	}

	public String getPreviousCiClaimExist() {
		return previousCiClaimExist;
	}

	public void setPreviousCiClaimExist(String previousCiClaimExist) {
		this.previousCiClaimExist = previousCiClaimExist;
	}

	public String getPreviousTpdClaimExist() {
		return previousTpdClaimExist;
	}

	public void setPreviousTpdClaimExist(String previousTpdClaimExist) {
		this.previousTpdClaimExist = previousTpdClaimExist;
	}

	public String getPreviousHospitalisationClaimExist() {
		return previousHospitalisationClaimExist;
	}

	public void setPreviousHospitalisationClaimExist(String previousHospitalisationClaimExist) {
		this.previousHospitalisationClaimExist = previousHospitalisationClaimExist;
	}

	public String getBenefitDescription() {
		return benefitDescription;
	}

	public void setBenefitDescription(String benefitDescription) {
		this.benefitDescription = benefitDescription;
	}

	public double getBenefitAmount() {
		return benefitAmount;
	}

	public void setBenefitAmount(double benefitAmount) {
		this.benefitAmount = benefitAmount;
	}

	public double getTotalPayableBenefit() {
		return totalPayableBenefit;
	}

	public void setTotalPayableBenefit(double totalPayableBenefit) {
		this.totalPayableBenefit = totalPayableBenefit;
	}

	public double getHcidConfirmationAmount() {
		return hcidConfirmationAmount;
	}

	public void setHcidConfirmationAmount(double hcidConfirmationAmount) {
		this.hcidConfirmationAmount = hcidConfirmationAmount;
	}

	public Date getHcidConfirmationDate() {
		return hcidConfirmationDate;
	}

	public void setHcidConfirmationDate(Date hcidConfirmationDate) {
		this.hcidConfirmationDate = hcidConfirmationDate;
	}

	public String getHospitalAddress() {
		return hospitalAddress;
	}

	public void setHospitalAddress(String hospitalAddress) {
		this.hospitalAddress = hospitalAddress;
	}

	public String getHospitalCity() {
		return hospitalCity;
	}

	public void setHospitalCity(String hospitalCity) {
		this.hospitalCity = hospitalCity;
	}

	public String getMedicalReportDetail() {
		return medicalReportDetail;
	}

	public void setMedicalReportDetail(String medicalReportDetail) {
		this.medicalReportDetail = medicalReportDetail;
	}

	public Date getMedicalReportReceivedDateTime() {
		return medicalReportReceivedDateTime;
	}

	public void setMedicalReportReceivedDateTime(Date medicalReportReceivedDateTime) {
		this.medicalReportReceivedDateTime = medicalReportReceivedDateTime;
	}

	public Date getMedicalReportVerifiedDateTime() {
		return medicalReportVerifiedDateTime;
	}

	public void setMedicalReportVerifiedDateTime(Date medicalReportVerifiedDateTime) {
		this.medicalReportVerifiedDateTime = medicalReportVerifiedDateTime;
	}

	public Date getAccidentDate() {
		return accidentDate;
	}

	public void setAccidentDate(Date accidentDate) {
		this.accidentDate = accidentDate;
	}

	public String getAccidentLocation() {
		return accidentLocation;
	}

	public void setAccidentLocation(String accidentLocation) {
		this.accidentLocation = accidentLocation;
	}

	public String getNatureOfInjury() {
		return natureOfInjury;
	}

	public void setNatureOfInjury(String natureOfInjury) {
		this.natureOfInjury = natureOfInjury;
	}

	public String getCircumstancesOfAccident() {
		return circumstancesOfAccident;
	}

	public void setCircumstancesOfAccident(String circumstancesOfAccident) {
		this.circumstancesOfAccident = circumstancesOfAccident;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	public String getDoctorQualification() {
		return doctorQualification;
	}

	public void setDoctorQualification(String doctorQualification) {
		this.doctorQualification = doctorQualification;
	}

	public String getDoctorSpeciality() {
		return doctorSpeciality;
	}

	public void setDoctorSpeciality(String doctorSpeciality) {
		this.doctorSpeciality = doctorSpeciality;
	}

	public String getPreExistingConditionDetail() {
		return preExistingConditionDetail;
	}

	public void setPreExistingConditionDetail(String preExistingConditionDetail) {
		this.preExistingConditionDetail = preExistingConditionDetail;
	}

	public String getPreExistingCondition() {
		return preExistingCondition;
	}

	public void setPreExistingCondition(String preExistingCondition) {
		this.preExistingCondition = preExistingCondition;
	}

	public String getDiagnosticDetail() {
		return diagnosticDetail;
	}

	public void setDiagnosticDetail(String diagnosticDetail) {
		this.diagnosticDetail = diagnosticDetail;
	}

	public Date getSymptomDate() {
		return symptomDate;
	}

	public void setSymptomDate(Date symptomDate) {
		this.symptomDate = symptomDate;
	}

	public Date getDiagnoseDate() {
		return diagnoseDate;
	}

	public void setDiagnoseDate(Date diagnoseDate) {
		this.diagnoseDate = diagnoseDate;
	}
	
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getPreviousDeathClaimExist() {
		return previousDeathClaimExist;
	}

	public void setPreviousDeathClaimExist(String previousDeathClaimExist) {
		this.previousDeathClaimExist = previousDeathClaimExist;
	}

	public Date getAppealedDate() {
		return appealedDate;
	}

	public void setAppealedDate(Date appealedDate) {
		this.appealedDate = appealedDate;
	}

	public String getIllnessType() {
		return illnessType;
	}

	public void setIllnessType(String illnessType) {
		this.illnessType = illnessType;
	}

	@Override
	public String toString() {
		return "Claim [id=" + id + ", policyId=" + policyId + ", type=" + type + ", claimStatus=" + claimStatus
				+ ", date=" + date + ", docCompletedBy=" + docCompletedBy + ", source=" + source + ", priority="
				+ priority + ", claiment=" + claiment + ", entitlement=" + entitlement + ", socialIdReceivedDate="
				+ socialIdReceivedDate + ", socialIdVerifiedDate=" + socialIdVerifiedDate + ", formReceivedDate="
				+ formReceivedDate + ", formVerifiedDate=" + formVerifiedDate + ", proposedBy=" + proposedBy
				+ ", proposedDate=" + proposedDate + ", claimApproved=" + claimApproved + ", approvedBy=" + approvedBy
				+ ", approvalDateTime=" + approvalDateTime + ", status=" + status + ", claimAmountPaid="
				+ claimAmountPaid + ", claimAmountTransferDate=" + claimAmountTransferDate + ", primaryReasonDelay="
				+ primaryReasonDelay + ", cancellationDate=" + cancellationDate + ", secondaryReasonDelay="
				+ secondaryReasonDelay + ", cancellationReason=" + cancellationReason + ", appealedLetterReceivedDate="
				+ appealedLetterReceivedDate + ", claimClosedDateTime=" + claimClosedDateTime + ", appealed=" + appealed
				+ ", investigationRequired=" + investigationRequired + ", investigationDetail=" + investigationDetail
				+ ", investigationCompletionDate=" + investigationCompletionDate + ", investigationOutcomeDetails="
				+ investigationOutcomeDetails + ", medicalReportConditionMet=" + medicalReportConditionMet
				+ ", hospitalName=" + hospitalName + ", hospitalPhoneNumber=" + hospitalPhoneNumber
				+ ", hospitalProvince=" + hospitalProvince + ", hospitalPostalCode=" + hospitalPostalCode
				+ ", hospitalCountry=" + hospitalCountry + ", marriageCertificationReceivedDate="
				+ marriageCertificationReceivedDate + ", marriageCertificationVerifiedDate="
				+ marriageCertificationVerifiedDate + ", numberOfPreviousHospitalisation="
				+ numberOfPreviousHospitalisation + ", previousCiClaimExist=" + previousCiClaimExist
				+ ", previousTpdClaimExist=" + previousTpdClaimExist + ", previousHospitalisationClaimExist="
				+ previousHospitalisationClaimExist + ", benefitDescription=" + benefitDescription + ", benefitAmount="
				+ benefitAmount + ", totalPayableBenefit=" + totalPayableBenefit + ", hcidConfirmationAmount="
				+ hcidConfirmationAmount + ", hcidConfirmationDate=" + hcidConfirmationDate + ", hospitalAddress="
				+ hospitalAddress + ", hospitalCity=" + hospitalCity + ", medicalReportDetail=" + medicalReportDetail
				+ ", medicalReportReceivedDateTime=" + medicalReportReceivedDateTime
				+ ", medicalReportVerifiedDateTime=" + medicalReportVerifiedDateTime + ", accidentDate=" + accidentDate
				+ ", accidentLocation=" + accidentLocation + ", natureOfInjury=" + natureOfInjury
				+ ", circumstancesOfAccident=" + circumstancesOfAccident + ", doctorName=" + doctorName
				+ ", doctorQualification=" + doctorQualification + ", doctorSpeciality=" + doctorSpeciality
				+ ", preExistingConditionDetail=" + preExistingConditionDetail + ", preExistingCondition="
				+ preExistingCondition + ", diagnosticDetail=" + diagnosticDetail + ", symptomDate=" + symptomDate
				+ ", diagnoseDate=" + diagnoseDate + ", previousDeathClaimExist=" + previousDeathClaimExist + "]";
	}
	

	 
	
}
