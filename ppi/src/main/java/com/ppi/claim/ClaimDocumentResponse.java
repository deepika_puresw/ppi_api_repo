package com.ppi.claim;


import com.ppi.core.Utilities;

public class ClaimDocumentResponse {

	private int id;
	
	private String name;
	
	private String date;
	
	private String url;
	
	/*public ClaimDocumentResponse(ClaimDocuments claimDocuments) {
	
		this.id=claimDocuments.getId();
		this.name=claimDocuments.getName();
		this.date=Utilities.dateToString(claimDocuments.getUplodedDate());
		this.url=claimDocuments.getUrl();
	}
*/
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	
}
