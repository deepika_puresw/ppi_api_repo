package com.ppi.claim;

import java.util.Date;

public class ClaimViewResponse {
	private int claimId;
	private String claimType;
	private String claimStatus;
	private Date docCompletionDate;
	private Date reportedDate;
	
	ClaimViewResponse(int claimId,String claimType,String claimStatus,Date docCompletionDate,Date reported_date){
		this.claimId=claimId;
		this.claimType=claimType;
		this.claimStatus=claimStatus;
		this.docCompletionDate=docCompletionDate;
		this.reportedDate=reported_date;
	}
	
	







	public int getClaimId() {
		return claimId;
	}









	public void setClaimId(int claimId) {
		this.claimId = claimId;
	}









	public Date getReportedDate() {
		return reportedDate;
	}





	public void setReportedDate(Date reportedDate) {
		this.reportedDate = reportedDate;
	}





	public String getClaimType() {
		return claimType;
	}
	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}
	public String getClaimStatus() {
		return claimStatus;
	}
	public void setClaimStatus(String claimStatus) {
		this.claimStatus = claimStatus;
	}
	public Date getDocCompletionDate() {
		return docCompletionDate;
	}
	public void setDocCompletionDate(Date docCompletionDate) {
		this.docCompletionDate = docCompletionDate;
	}
	

}
