package com.ppi.customer;


	

	import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
	import javax.persistence.Entity;
	import javax.persistence.GeneratedValue;
	import javax.persistence.GenerationType;
	import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;  

	@Entity
	@Table(name="ppi_policy")  
	public class Policy { 
		
		@Id
		 @GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name="id")
		private int id; 

		
		@Column(name="claim_id")
		private int claim_id; 

		@Column(name="customer_id")
		private int customer_id;
		
		@Column(name="contract_id")
		private int contract_id;

		
		@Column(name="active_date")
		private Date active_date;

		@Column(name="tenure")
		private int tenure;
		
		@Column(name="credit_amount")
		private double credit_amount;
		
		@Column(name="monthly_ins_amount")
		private double monthly_ins_amount;
		
		@Column(name="prod_code_hcid")
		private String prod_code_hcid;
		
		
		@Column(name="fst_ins_due_date")
		private Date fst_ins_due_date;
	
		
		@Column(name="installment_rate")
		private double installment_rate;
		
		@Column(name="last_ins_due_date")
		private Date last_ins_due_date;
		
		
		@Column(name="resgis_status")
		private String resgis_status;
		
		
		@Column(name="sales_id")
		private int sales_id;
		
		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public int getClaim_id() {
			return claim_id;
		}

		public void setClaim_id(int claim_id) {
			this.claim_id = claim_id;
		}

		public int getCustomer_id() {
			return customer_id;
		}

		public void setCustomer_id(int customer_id) {
			this.customer_id = customer_id;
		}

		public int getContract_id() {
			return contract_id;
		}

		public void setContract_id(int contract_id) {
			this.contract_id = contract_id;
		}

		public Date getActive_date() {
			return active_date;
		}

		public void setActive_date(Date active_date) {
			this.active_date = active_date;
		}

		public int getTenure() {
			return tenure;
		}

		public void setTenure(int tenure) {
			this.tenure = tenure;
		}

		public double getCredit_amount() {
			return credit_amount;
		}

		public void setCredit_amount(double credit_amount) {
			this.credit_amount = credit_amount;
		}

		public double getMonthly_ins_amount() {
			return monthly_ins_amount;
		}

		public void setMonthly_ins_amount(double monthly_ins_amount) {
			this.monthly_ins_amount = monthly_ins_amount;
		}

		public String getProd_code_hcid() {
			return prod_code_hcid;
		}

		public void setProd_code_hcid(String prod_code_hcid) {
			this.prod_code_hcid = prod_code_hcid;
		}

		/*public Date getFirst_ins_due_date() {
			return first_ins_due_date;
		}

		public void setFirst_ins_due_date(Date first_ins_due_date) {
			this.first_ins_due_date = first_ins_due_date;
		}*/

		public double getInstallment_rate() {
			return installment_rate;
		}

		public Date getFst_ins_due_date() {
			return fst_ins_due_date;
		}

		public void setFst_ins_due_date(Date fst_ins_due_date) {
			this.fst_ins_due_date = fst_ins_due_date;
		}

		public void setInstallment_rate(double installment_rate) {
			this.installment_rate = installment_rate;
		}

		public Date getLast_ins_due_date() {
			return last_ins_due_date;
		}

		public void setLast_ins_due_date(Date last_ins_due_date) {
			this.last_ins_due_date = last_ins_due_date;
		}

		public String getResgis_status() {
			return resgis_status;
		}

		public void setResgis_status(String resgis_status) {
			this.resgis_status = resgis_status;
		}

		public int getSales_id() {
			return sales_id;
		}

		public void setSales_id(int sales_id) {
			this.sales_id = sales_id;
		}

		public String getCreated_by() {
			return created_by;
		}

		public void setCreated_by(String created_by) {
			this.created_by = created_by;
		}

		public Date getCreated_on() {
			return created_on;
		}

		public void setCreated_on(Date created_on) {
			this.created_on = created_on;
		}

		@Column(name="created_by")
		private String created_by;
		
		@Column(name="created_on")
		private Date created_on;
		
		@Column(name="iddNo")
		private int iddNo;
		
		

		

		public int getIddNo() {
			return iddNo;
		}

		public void setIddNo(int iddNo) {
			this.iddNo = iddNo;
		}

		public int getPolicyNo() {
			return policyNo;
		}

		public void setPolicyNo(int policyNo) {
			this.policyNo = policyNo;
		}

		@Column(name="policyNo")
		private int policyNo;
		
		
		//@OneToMany(mappedBy = "ppi_customer", cascade = CascadeType.ALL)
		 
		@ManyToOne(optional=false)
	    @JoinColumn(name="customer_id",referencedColumnName="id", insertable = false, updatable = false)
	    private Customer customer;

		public Customer getCustomer() {
			return customer;
		}

		public void setCustomer(Customer customer) {
			this.customer = customer;
		}
		
		
		
		 
		@ManyToOne(optional=false)
	    @JoinColumn(name="sales_id",referencedColumnName="id", insertable = false, updatable = false)
	    private Sale sales;

		public Sale getSales() {
			return sales;
		}

		public void setSales(Sale sales) {
			this.sales = sales;
		}

		
}
