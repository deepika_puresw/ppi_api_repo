package com.ppi.customer;


import java.util.List;

public interface customerService  {
	

	
	List<CustomerDetailsResponse> customerDetailsInformation(int contract_id);
	
	List<PolicyDetailsResponse> policyDetailsInformation(int contract_id);

	List<LoanDetailsResponse> loanDetailsInformation(int contract_id);

	List<SalesDetailsResponse> salesDetailsInformation(int policy_id);
	List<CustomerResponse> getPolicySearchDetails(int policyNo,int social_id,int contract_id,String name,int mob_no,String email);

	List<CustomerResponse> getCustomersSearch(int contract_id, String name, int social_id, int mob_no);
	
	

	
	


	
	
}
