package com.ppi.customer;

import java.util.ArrayList;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
 
import java.util.List;

@Entity
@Table(name="ppi_customer")  
public class Customer { 
	
	 
	@Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id; 


	@Column(name="name")
	private String name; 

	@Column(name="mob_no")
	private int mob_no;
	
	@Column(name="date_birth")
	private Date date_birth;

	
	@Column(name="gender")
	private String gender;

	@Column(name="email")
	private String email;
	
	@Column(name="social_id")
	private int social_id;
	
	@Column(name="cuid")
	private int cuid;
	
	@Column(name="virtual_acc")
	private int virtual_acc;
	
	
	@Column(name="address")
	private String address;
	
	 
	 //private Policy policy;
	//private String policy_id;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	public Date getDate_birth() {
		return date_birth;
	}

	public void setDate_birth(Date date_birth) {
		this.date_birth = date_birth;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getSocial_id() {
		return social_id;
	}

	public void setSocial_id(int social_id) {
		this.social_id = social_id;
	}

	public int getCuid() {
		return cuid;
	}

	public void setCuid(int cuid) {
		this.cuid = cuid;
	}

	public int getVirtual_acc() {
		return virtual_acc;
	}

	public void setVirtual_acc(int virtual_acc) {
		this.virtual_acc = virtual_acc;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMob_no() {
		return mob_no;
	}

	public void setMob_no(int mob_no) {
		this.mob_no = mob_no;
	}

	/*public Policy getPolicy() {
		return policy;
	}

	public void setPolicy(Policy policy) {
		this.policy = policy;
	}*/

	

	

	
	@OneToMany(mappedBy="customer",targetEntity=Policy.class, fetch=FetchType.EAGER)
    private List<Policy> policy;
	public List<Policy> getPolicy() {
		
		return policy;
	}

	public void setPolicy(List<Policy> policy) {
		this.policy = policy;
	}
	/*@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id")
    public int getPolicy() {
        return getPolicy();
    }
 
    public void setPolicy(Policy policy) {
        this.policy = policy;
    }*/
}

