package com.ppi.customer;

import java.util.Date;

public class CustomerDetailsResponse {
    private String firstName;
    private String lastName;
	private Date dob;
	private int mob_no;
	private int contract_id;
	private int social_id;
	private String email;
	private int policy_id;
	private String gender;
	private int alternate_ph_Number;
	private String bank_name;
	private int bank_acc_Number;
	String language;
	String eligibility_status;
	String Currency;
	String maritalStatus;
	String address;
	String city;
	String province;
	int postalCode;
	String country;
	
	
	
	public CustomerDetailsResponse(String firstName,String lastName,Date dob,int mob_no,int social_id,String email,int policy_id,String gender,int alternate_ph_Number,String bank_name,int bank_acc_Number,String language,String eligibility_status,String Currency,String maritalStatus,String address,String city,String province,int postalCode,String country){
		this.firstName=firstName;
		this.lastName=lastName;
		this.dob=dob;
		this.bank_acc_Number=bank_acc_Number;
		this.mob_no=mob_no;
		this.gender=gender;
		//this.contract_id=contract_id;
		this.social_id=social_id;
		this.email=email;
		this.policy_id=policy_id;
		this.alternate_ph_Number=alternate_ph_Number;
		this.bank_name=bank_name;
		this.language=language;
		this.eligibility_status=eligibility_status;
		this.Currency=Currency;
		this.maritalStatus=maritalStatus;
		this.address=address;
		this.city=city;
		this.province=province;
		this.postalCode=postalCode;
		this.country=country;
	}
	
	public int getPolicy_id() {
		return policy_id;
	}

	/*public void setPolicy_id(int policy_id) {
		this.policy_id = policy_id;
	}
*/
	public CustomerDetailsResponse() {
		// TODO Auto-generated constructor stub
	}
	
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getAlternate_ph_Number() {
		return alternate_ph_Number;
	}

	public void setAlternate_ph_Number(int alternate_ph_Number) {
		this.alternate_ph_Number = alternate_ph_Number;
	}

	public String getBank_name() {
		return bank_name;
	}

	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}

	public int getBank_acc_Number() {
		return bank_acc_Number;
	}

	public void setBank_acc_Number(int bank_acc_Number) {
		this.bank_acc_Number = bank_acc_Number;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getEligibility_status() {
		return eligibility_status;
	}

	public void setEligibility_status(String eligibility_status) {
		this.eligibility_status = eligibility_status;
	}

	public String getCurrency() {
		return Currency;
	}

	public void setCurrency(String currency) {
		Currency = currency;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public int getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(int postalCode) {
		this.postalCode = postalCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setPolicy_id(int policy_id) {
		this.policy_id = policy_id;
	}

	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public int getMob_no() {
		return mob_no;
	}
	public void setMob_no(int mob_no) {
		this.mob_no = mob_no;
	}
	
	public int getContract_id() {
		return contract_id;
	}
	public void setContract_id(int contract_id) {
		this.contract_id = contract_id;
	}
	public int getSocial_id() {
		return social_id;
	}
	public void setSocial_id(int social_id) {
		this.social_id = social_id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
}
