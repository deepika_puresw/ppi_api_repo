package com.ppi.customer;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


import com.ps.webtest.rest.PhoneIntegrationService;

import io.swagger.annotations.*;



public class customerController {
	
	 private final Logger logger = Logger.getLogger(customerController.class);

	@Autowired
    customerService customerService;
	
	
  @POST
    @Path("getPolicySearch")
	 @Produces(MediaType.APPLICATION_JSON)
	 @Consumes(MediaType.APPLICATION_JSON)
	public @ResponseBody Map<String, Object> postController(@RequestBody CustomerResponse cr) {
	    HashMap<String,Object> map=new HashMap<String, Object>();
	   int customer_id=0;
	   // map.put("message", new msgRes(200,"successful","ok"));
	 
	    map.put("customer",customerService.getPolicySearchDetails(cr.getPolicyNo(),cr.getSocial_id(),cr.getContract_id(),cr.getName(),cr.getMob_no(),cr.getEmail()));
	   
	    
		    
    	return map;
    	
	}

  @GET
  @Path("viewPolicyDetails/{id}")
	 @Produces(MediaType.APPLICATION_JSON)
	
	public @ResponseBody Map<String, Object> policyDetails(@PathParam("id") int policy_id) {
	    HashMap<String,Object> map=new HashMap<String, Object>();
	   int customer_id=0;
	   // map.put("message", new msgRes(200,"successful","ok"));
	    int sales_id=0;
	    map.put("customer Information",customerService.customerDetailsInformation(policy_id));
	    map.put("Policy Information",customerService.policyDetailsInformation(policy_id));
	   
	    map.put("Loan Information",customerService.loanDetailsInformation(policy_id));
	    map.put("Sales Information",customerService.salesDetailsInformation(policy_id));
	    	
	    
  	return map;
	}

  
  @POST
  @Path("searchCustomer")
	 @Produces(MediaType.APPLICATION_JSON)
	 @Consumes(MediaType.APPLICATION_JSON)
	public @ResponseBody Map<String, Object> searchCustomer(@RequestBody CustomerResponse cr) {
	    HashMap<String,Object> map=new HashMap<String, Object>();
	   	    map.put("customer",customerService.getCustomersSearch(cr.getContract_id(),cr.getName(),cr.getSocial_id(),cr.getMob_no()));
		   
		  
  	return map;
	} 
  
  
  @GET
  @Path("viewCustomerDetails/{id}")
	 @Produces(MediaType.APPLICATION_JSON)
	
	public @ResponseBody Map<String, Object> CustomerInfoDetails(@PathParam("id") int policy_id) {
	    HashMap<String,Object> map=new HashMap<String, Object>();
	     map.put("customer Information",customerService.customerDetailsInformation(policy_id));
	    
	    
  	return map;
	}

  
  
  
}