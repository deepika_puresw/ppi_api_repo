package com.ppi.customer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

//@ApiModel(value = "CustomerResponse", description = "getting customer details as response")
public class CustomerResponse {
	
	//@ApiModelProperty(value = "Customer Id", required = true)
	private String name;;
	
	
	

	public int getPolicy_id() {
		return policy_id;
	}

	/*public void setPolicy_id(int policy_id) {
		this.policy_id = policy_id;
	}*/

	public void setProd_code(String prod_code) {
		this.prod_code = prod_code;
	}

	//@ApiModelProperty(value = "customer mobile number", required = true)
	private int mob_no;
	
	private String prod_code;
	
	private int contract_id;
	private int social_id;
	private String email;
	private int policy_id;
	private String gender;
	private String pol_status;
	
	private int policyNo;
	
	

	

	public int getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(int policyNo) {
		this.policyNo = policyNo;
	}

	public int getSocial_id() {
		return social_id;
	}

	public void setSocial_id(int social_id) {
		this.social_id = social_id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getContract_id() {
		return contract_id;
	}

	public void setContract_id(int contract_id) {
		this.contract_id = contract_id;
	}

	public String getProd_code() {
		return prod_code;
	}

	public void setPolicy_id(String prod_code) {
		this.prod_code = prod_code;
	}

	public CustomerResponse(String name,int mob_no,int contract_id,int social_id,String email,String prod_code,int policy_id,String gender,String pol_status,int policyNo){
		this.name=name;
		this.prod_code=prod_code;
		this.mob_no=mob_no;
		this.contract_id=contract_id;
		this.social_id=social_id;
		this.email=email;
		this.policy_id=policy_id;
		this.gender=gender;
		this.pol_status=pol_status;
		
		this.policyNo=policyNo;
	}
	
	public String getPol_status() {
		return pol_status;
	}

	public void setPol_status(String pol_status) {
		this.pol_status = pol_status;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public CustomerResponse() {
		// TODO Auto-generated constructor stub
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	


	public int getMob_no() {
		return mob_no;
	}

	public void setMob_no(int mob_no) {
		this.mob_no = mob_no;
	}

	
}
