package com.ppi.customer;

import java.util.Date;

public class LoanDetailsResponse {
	private double credit_amount;
private double premium_amount;
private Date first_instal_due_date;
private Date last_instal_due_date;
private double monthly_installment_amount;
private double installment_rate;

	
     LoanDetailsResponse(double credit_amount,double premium_amount,Date first_instal_due_date,Date last_instal_due_date,double monthly_installment_amount,double installment_rate){
		this.credit_amount=credit_amount;
		this.premium_amount=premium_amount;
		this.first_instal_due_date=first_instal_due_date;
		this.last_instal_due_date=last_instal_due_date;
		this.monthly_installment_amount=monthly_installment_amount;
		this.installment_rate=installment_rate;
	}
	
	public double getPremium_amount() {
		return premium_amount;
	}

	public void setPremium_amount(double premium_amount) {
		this.premium_amount = premium_amount;
	}

	public Date getFirst_instal_due_date() {
		return first_instal_due_date;
	}

	public void setFirst_instal_due_date(Date first_instal_due_date) {
		this.first_instal_due_date = first_instal_due_date;
	}

	public Date getLast_instal_due_date() {
		return last_instal_due_date;
	}

	public void setLast_instal_due_date(Date last_instal_due_date) {
		this.last_instal_due_date = last_instal_due_date;
	}

	public double getMonthly_installment_amount() {
		return monthly_installment_amount;
	}

	public void setMonthly_installment_amount(double monthly_installment_amount) {
		this.monthly_installment_amount = monthly_installment_amount;
	}

	public double getInstallment_rate() {
		return installment_rate;
	}

	public void setInstallment_rate(double installment_rate) {
		this.installment_rate = installment_rate;
	}

	public double getCredit_amount() {
		return credit_amount;
	}

	public void setCredit_amount(double credit_amount) {
		this.credit_amount = credit_amount;
	}
	

}
