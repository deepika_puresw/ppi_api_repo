package com.ppi.customer;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ps.webtest.database.dao.AbstractDao;



@Repository
public class customerDaoImpl implements customerDao {

	@Autowired
	SessionFactory sf;
	
	
	@Override
    public List<Customer> getCustomerDetails(int policy_id) {
		Session session=sf.getCurrentSession();      
     		Query query = null;
     		query = session.createQuery("select c from Customer c "
 					+ "JOIN c.policy p " 
 	             	+ "WHERE p.id = '"+policy_id +"' ");
 	           	    
 		
     		List<Customer> list = query.list();
     		return list;
	}

	@Override
    public List<Policy> polDetails(int policy_id) {
		Session session=sf.getCurrentSession();      
     		Query query = null;
     		query = session.createQuery("select p from Policy p "
 					
 	             	+ "WHERE p.id = '"+policy_id +"' ");
 	           	    
 		
     		List<Policy> list = query.list();
     		return list;
	}
	
	@Override
    public List<Sale> getsalesdetails(int policy_id) {
		Session session=sf.getCurrentSession();      
     		Query query = null;
     		
     			query = session.createQuery("select s from Sale s "
     					+ "JOIN s.policy p " 
     	             	+ "WHERE p.id = '"+policy_id +"' ");
     	             	
     			List<Sale> list = query.list();
         		return list;
     		
     		}
	
	
	@SuppressWarnings("unused")
	@Override
	public List<Customer> getCustomersOnpolicySearch(int policyNo, int social_id, int contract_id, String name,
			int mob_no, String email) {
		String query1 = null;
		String query2=null;
		String query3=null;
		Query query4=null;
		Session session= sf.getCurrentSession(); 
		
		query1 = joinQueryString()+  " p.policyNo = '"+policyNo +"' ";
		query2 = joinQueryString()+  " p.contract_id = '"+contract_id +"' ";
		query3 = joinQueryString()+  " p.policyNo = '"+policyNo +"' "
             	+  " AND p.contract_id= '"+contract_id +"' ";
		if(policyNo!=0&&contract_id==0) {
			if(name!=null) {
				query1 =  query1 + " and c.name= '"+name +"' ";
	 		
	 		}
			if(email!=null) {
				query1 = query1 + " and c.email= '"+email +"' ";
	 					 		
	 		}
			if(mob_no!=0) {
				query1 = query1 + " and c.mob_no= '"+mob_no +"' ";
	 					 		
	 		}
			if(social_id!=0) {
				query1 = query1 + " and c.social_id= '"+social_id +"' ";
	 					 		
	 		}
			 query4= session.createQuery(query1);
			
		}
		if(policyNo==0&&contract_id!=0) {
			if(name!=null) {
				query2 =  query2 + "and c.name= '"+name +"' ";
	 		
	 		}
			if(email!=null) {
				query2 = query2 + " and c.email= '"+email +"' ";
	 					 		
	 		}
			if(mob_no!=0) {
				query2 = query2 + " and c.mob_no= '"+mob_no +"' ";
	 					 		
	 		}
			if(social_id!=0) {
				query2 = query2 + " and c.social_id= '"+social_id +"' ";
	 					 		
	 		}
			 query4= session.createQuery(query2);
			
		}
		
		if(social_id==0&&mob_no==0&&name==null&&email==null&&policyNo!=0&&contract_id!=0) {
			if(name!=null) {
				query3 =  query3 + " and c.name= '"+name +"' ";
	 		
	 		}
			if(email!=null) {
				query3 = query3 + " and c.email= '"+email +"' ";
	 					 		
	 		}
			if(mob_no!=0) {
				query3 = query3 + " and c.mob_no= '"+mob_no +"' ";
	 					 		
	 		}
			if(social_id!=0) {
				query3 = query3 + " and c.social_id= '"+social_id +"' ";
	 					 		
	 		}
			query4= session.createQuery(query3);
		}
		if(policyNo==0&&contract_id==0) {
			String query5=getCustomeDetailsOnpolicyquery(policyNo,social_id,contract_id,name,mob_no,email);
			query4 = session.createQuery(query5);
			
		}
		List<Customer> list = query4.list();
 		return list;
	}
	
	 public String joinQueryString() {
		 String joinString=getCustomeDetailsselectConditions()
					+ "JOIN c.policy p " 
	             	+ "WHERE";
		 return joinString;
	 }
	 
	 
	public String getCustomeDetailsselectConditions() {
		String select = "select c from Customer c  " ;
		return select;
	}

   public String getCustomeDetailsOnpolicyquery(int policyNo, int social_id, int contract_id, String name,
			int mob_no, String email) {
	        String query = getCustomeDetailsselectConditions()+"where";
		
		if(name!=null) {
 			query =  query + " c.name= '"+name +"' ";
 		
 		}
		if(email!=null) {
 			query = query + " and c.email= '"+email +"' ";
 					 		
 		}
		if(mob_no!=0) {
 			query = query + " and c.mob_no= '"+mob_no +"' ";
 					 		
 		}
		if(social_id!=0) {
 			query = query + " and c.social_id= '"+social_id +"' ";
 					 		
 		}
		return query;
	
}
   
   @Override
   public List<Customer> getCustomerSeachList(int contract_id, String name, int social_id, int mob_no) {
	   
	   String query1 = null;
		Query query4=null;
		Session session= sf.getCurrentSession(); 
		query1 = getCustomeDetailsselectConditions()
					+ "JOIN c.policy p " 
	             	+ "WHERE p.contract_id = '"+contract_id +"' ";
		if((contract_id!=0)){
			String query2=query1;
			if(name!=null) {
				query2 = query2 + " and c.name= '"+name +"' ";
	 		
	 		}
			
			if(mob_no!=0) {
				query2 = query2 + " and c.mob_no= '"+mob_no +"' ";
	 					 		
	 		}
			if(social_id!=0) {
				query2 = query2 + " and c.social_id= '"+social_id +"' ";
	 					 		
	 		}
			 query4= session.createQuery(query2);
			
		}
	   if(contract_id==0) {
		   String query = getCustomeDetailsselectConditions()+"where";
			//String query1 = null;
			
			if(name!=null) {
	 			query =  query + " c.name= '"+name +"' ";
	 		
	 		}
		
			if(mob_no!=0) {
	 			query = query + " and c.mob_no= '"+mob_no +"' ";
	 					 		
	 		}
			if(social_id!=0) {
	 			query = query + " and c.social_id= '"+social_id +"' ";
	 					 		
	 		}
			query4= session.createQuery(query);
	   }
		List<Customer> list = query4.list();
 		return list;
}
}
	