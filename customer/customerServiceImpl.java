package com.ppi.customer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class customerServiceImpl implements customerService {
	@Autowired
	public customerDaoImpl customerDaoImpl;
	
	
	
	

	@Override
	@Transactional
	public List<CustomerDetailsResponse> customerDetailsInformation(int policy_id) {
		List<CustomerDetailsResponse> cust = new ArrayList<>();
		
		List<Customer> customerList = customerDaoImpl.getCustomerDetails(policy_id);
		String lastName=null;
		int Id = 0;
		String gender=null;
		String prod_code=null;
		int alternatePhNum=0;
		String bank_name=null;
		int bank_acc_No=0;
		String language=null;
		String eligibilityStaus=null;
		String currency=null;
		String maritalStatus=null;
		String address=null;
		String city=null;
		String province=null;
		int postalCode=0;
		String country=null;
		for (Customer a : customerList) {
        for (Policy policy : a.getPolicy()) {
				
        	Id = policy.getId();
				
				prod_code=policy.getProd_code_hcid();
             }
			cust.add(new CustomerDetailsResponse(a.getName(),lastName,a.getDate_birth(),a.getMob_no(),a.getSocial_id(),a.getEmail(),Id,gender,alternatePhNum,bank_name,bank_acc_No,language,eligibilityStaus,currency,maritalStatus,address,city,province,postalCode,country));

						
		}
		return cust;
	}

	@Override
	@Transactional
	public List<PolicyDetailsResponse> policyDetailsInformation(int policy_id) {
		List<PolicyDetailsResponse> cust = new ArrayList<>();
		
		List<Policy> customerList = customerDaoImpl.polDetails(policy_id);
		Date policy_start_date=null;
		Date policy_end_date=null;
		int contractId = 0;
		int remaining_tenure=0;
		String prod_code=null;
		Date cancelletion_date=null;
		Date termination_date=null;
		for (Policy a : customerList) {
        
			cust.add(new PolicyDetailsResponse(a.getProd_code_hcid(),a.getContract_id(),a.getResgis_status(),policy_start_date,policy_end_date,a.getTenure(),remaining_tenure,cancelletion_date,termination_date));
		
		}
		return cust;
	}

	
	@Override
	@Transactional
	public List<LoanDetailsResponse> loanDetailsInformation(int policy_id) {
		List<LoanDetailsResponse> cust = new ArrayList<>();
		
		List<Policy> customerList = customerDaoImpl.polDetails(policy_id);
		int premium_amount=0;
		
		for (Policy a : customerList) {
       
			cust.add(new LoanDetailsResponse(a.getCredit_amount(),premium_amount,a.getFst_ins_due_date(),a.getLast_ins_due_date(),a.getMonthly_ins_amount(),a.getInstallment_rate()));
			
			
		}
		return cust;
	}
	
	@Override
	@Transactional
	public List<SalesDetailsResponse> salesDetailsInformation(int policy_id) {
		List<SalesDetailsResponse> cust = new ArrayList<>();
		
		List<Sale> polList = customerDaoImpl.getsalesdetails(policy_id);
		
		
		for (Sale a : polList) {
       
			cust.add(new SalesDetailsResponse(a.getCity(),a.getName()));

			
			
		}
		return cust;
	}
	
	
	
	@Override
	@Transactional
	public List<CustomerResponse> getPolicySearchDetails(int policyNo,int social_id,int contract_id,String name,int mob_no,String email){
		List<CustomerResponse> cust = new ArrayList<>();
		  List<Customer> customerList = customerDaoImpl.getCustomersOnpolicySearch(policyNo,social_id,contract_id,name,mob_no,email);
		
		  int contractId = 0;
			int id=0;
			String prod_code=null;
			String pol_status=null;
			
			int policyNumber=0;
			for (Customer a : customerList) {
				
				for (Policy policy : a.getPolicy()) {
					
					contractId = policy.getContract_id();
					id=policy.getId();
					prod_code=policy.getProd_code_hcid();
					
					policyNumber=policy.getPolicyNo();
					
					pol_status=policy.getResgis_status();
					if(contract_id==contractId && policyNo==0)
					cust.add(new CustomerResponse(a.getName(),a.getMob_no(),contractId,a.getSocial_id(),a.getEmail(),prod_code,id,a.getGender(),pol_status,policyNumber));
					if(contractId==contract_id && policyNumber==policyNo)
						cust.add(new CustomerResponse(a.getName(),a.getMob_no(),contractId,a.getSocial_id(),a.getEmail(),prod_code,id,a.getGender(),pol_status,policyNumber));
					if(contract_id==0 && policyNo==policyNumber)
						cust.add(new CustomerResponse(a.getName(),a.getMob_no(),contractId,a.getSocial_id(),a.getEmail(),prod_code,id,a.getGender(),pol_status,policyNumber));
					if(contract_id==0 && policyNo==0)
						cust.add(new CustomerResponse(a.getName(),a.getMob_no(),contractId,a.getSocial_id(),a.getEmail(),prod_code,id,a.getGender(),pol_status,policyNumber));
					
				}
			}
			
		  return cust;
	}
	
	@Override
	@Transactional
	public List<CustomerResponse> getCustomersSearch(int contract_id, String name, int social_id, int mob_no){
		List<CustomerResponse> cust = new ArrayList<>();
		  List<Customer> customerList = customerDaoImpl.getCustomerSeachList(contract_id,name,social_id,mob_no);
		  int contractId = 0;
			int id=0;
			String prod_code=null;
			String pol_status=null;
			
			
			for (Customer a : customerList) {
				
				for (Policy policy : a.getPolicy()) {
					
					contractId = policy.getContract_id();
					id=policy.getId();
					prod_code=policy.getProd_code_hcid();
					
					int policyNo=policy.getPolicyNo();
					
					pol_status=policy.getResgis_status();
					if(contractId==contract_id || contract_id==0)
					cust.add(new CustomerResponse(a.getName(),a.getMob_no(),contractId,a.getSocial_id(),a.getEmail(),prod_code,id,a.getGender(),pol_status,policyNo));
				}
			}
		  
		  return cust;
	}
	
}
