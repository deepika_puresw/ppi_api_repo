package com.ppi.customer;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="ppi_sales")  
public class Sale { 
		 
		@Id
		 @GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name="id")
		private int id; 


		@Column(name="name")
		private String name; 

		@Column(name="city")
		private String city;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getCity() {
			return city;
		}

		public void setCity(String city) {
			this.city = city;
		}
		
		@OneToMany(mappedBy="sales",targetEntity=Policy.class, fetch=FetchType.EAGER)
	    private List<Policy> policy;
		public List<Policy> getPolicy() {
			
			return policy;
		}

		public void setPolicy(List<Policy> policy) {
			this.policy = policy;
		}
		
}
